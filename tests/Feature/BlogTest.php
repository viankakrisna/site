<?php

namespace Tests\Feature;

use App\Blog;
use TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BlogTest extends TestCase {

    public function testValidate() {
        $blog           = new Blog();
        $blog->name     = 'Test Blog';
        $blog->lang     = 'en';
        $blog->url      = 'https://www.acme.org';
        $blog->is_rss   = 1;
        $blog->feed_url = 'https://www.acme.org/feed';

        $this->assertTrue( $blog->validate() );
    }

    public function testValidateUrlWithoutProtocol() {
        $blog       = new Blog();
        $blog->name = 'Test Blog';
        $blog->lang = 'en';
        $blog->url  = 'www.acme.org';

        $this->assertFalse( $blog->validate() );
    }

    public function testValidateWhenHasUrlButNotFeedUrl() {
        $blog       = new Blog();
        $blog->name = 'Test Blog';
        $blog->lang = 'en';
        $blog->url  = 'https://www.acme.org';

        $this->assertTrue( $blog->validate() );
    }

    public function testShouldNotBeApprovedByDefault() {
        $blog           = new Blog();
        $blog->name     = 'Test Blog';
        $blog->lang     = 'en';
        $blog->url      = 'https://www.acme.org';
        $blog->is_rss   = 1;
        $blog->feed_url = 'https://www.acme.org/feed';

        $this->assertTrue( $blog->approved == 0 );
    }

    // @todo
    public function testExists() {
        $blog       = new Blog();
        $blog->name = 'Test Blog';
        $blog->lang = 'en';
        $blog->url  = 'https://www.acme.org';

        $this->assertTrue( true );
    }

}
