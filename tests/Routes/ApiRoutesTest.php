<?php

class ApiRoutesTest extends TestCase {
    protected $postController;
    protected $blogController;
    protected $adminBlogController;
    protected $adminStatsController;
    protected $adminLogController;
    protected $adminCronController;

    public function setUp() {
        parent::setUp();
        // Create mock
        $this->postController       = \Mockery::mock( 'App\Api\V1\Controllers\Pub\PostController[index,getTopPosters]' );
        $this->blogController       = \Mockery::mock( 'App\Api\V1\Controllers\Pub\BlogController[index,store,getTopBlogs,getBlogInfo]' );
        $this->adminBlogController  = \Mockery::mock( 'App\Api\V1\Controllers\Admin\AdminBlogController[index,delete,update]' );
        $this->adminStatsController = \Mockery::mock( 'App\Api\V1\Controllers\Admin\AdminStatsController[getStats]' );
        $this->adminLogController   = \Mockery::mock( 'App\Api\V1\Controllers\Admin\AdminLogController[getFetchLog]' );
        $this->adminCronController  = \Mockery::mock( 'App\Api\V1\Controllers\Admin\AdminCronController[getInfo]' );

        // Bind instance of controller to the mock
        App::instance( 'App\Api\V1\Controllers\Pub\PostController', $this->postController );
        App::instance( 'App\Api\V1\Controllers\Pub\BlogController', $this->blogController );
        App::instance( 'App\Api\V1\Controllers\Admin\AdminBlogController', $this->adminBlogController );
        App::instance( 'App\Api\V1\Controllers\Admin\AdminStatsController', $this->adminStatsController );
        App::instance( 'App\Api\V1\Controllers\Admin\AdminLogController', $this->adminLogController );
        App::instance( 'App\Api\V1\Controllers\Admin\AdminCronController', $this->adminCronController );
    }

    public function testRoutes() {
        // Posts
        $this->postController->shouldReceive( 'index' )->once();
        $this->call( 'GET', '/api/posts' );
        $this->postController->shouldReceive( 'getTopPosters' )->once();
        $this->call( 'GET', '/api/posts/top-posters' );

        // Blogs
        $this->blogController->shouldReceive( 'index' )->once();
        $this->call( 'GET', '/api/blogs' );
        $this->blogController->shouldReceive( 'store' )->once();
        $this->call( 'POST', '/api/blogs' );
        $this->blogController->shouldReceive( 'getTopBlogs' )->once();
        $this->call( 'GET', '/api/blogs/top-blogs' );
        $this->blogController->shouldReceive( 'getBlogInfo' )->once();
        $this->call( 'POST', '/api/blogs/blog-info' );

        // Admin
        $this->withoutMiddleware();

        // Admin - Blogs
        $this->adminBlogController->shouldReceive( 'index' )->once();
        $this->call( 'GET', '/api/admin/blogs' );
        $this->adminBlogController->shouldReceive( 'update' )->once();
        $this->call( 'PUT', '/api/admin/blogs/1' );
        $this->adminBlogController->shouldReceive( 'delete' )->once();
        $this->call( 'DELETE', '/api/admin/blogs/1' );

        // Admin - Stats
        $this->adminStatsController->shouldReceive( 'getStats' )->once();
        $this->call( 'POST', '/api/admin/stats/stats' );

        // Admin - Log
        $this->adminLogController->shouldReceive( 'getFetchLog' )->once();
        $this->call( 'POST', '/api/admin/logs/fetch' );

        // Admin - Cron
        $this->adminCronController->shouldReceive( 'getInfo' )->once();
        $this->call( 'POST', '/api/admin/cron/info' );

    }
}