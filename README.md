# Planet React

Planet React is an aggregation of blogs and news from the React community.

https://www.planet-react.org

## Hacking

Before setting up the project, you need to verify that the following software are installed:

* GIT
* MySQL
* PHP 7
* Node 6
* Yarn or NPM
* PHP Composer

Note: The project has only been tested on OS X


## Install (local)

Replace `{PLANET_INSTALL}` with your installation location (no brackets)

1) Create an installation directory and cd into it:

    mkdir planet-react && cd planet-react

2) Clone the repo:

    git clone https://gitlab.com/thomas.andersen/planet-react

3) Install the PHP and Node dependencies:

    composer install && yarn

4) Create the configuration file:

    cp .env.example .env

5) Open the .env file and set the correct `DB_*` settings.

6) Generate a Laravel application key:

    php artisan key:generate

7) Init the database:

    php artisan migrate


You should now be able to start the servers by typing `yarn start` [Enter]

Open the browser and navigate to [http://localhost:3000](http://localhost:3000)

Note that webpack needs to build the app the first time. Be patient!


### Commands

If you don' use Yarn, replace `Yarn` with `npm`

Start the webpack development server and PHP server

    yarn start

Run tests

    yarn test

Build production bundle

    yarn run build

Deploy to production (requires a web server / SSH key pairs. See .env)

    yarn run deploy

Start aggregation

    php artisan planet:fetch

Run database backup (requires Google Drive / API credentials. See .env)

    php artisan planet:db-backup


### Configuration

#### auth0

The `/admin/*` and `/api/admin/*` routes uses auth0 for authentication.
In order to make the authentication to work you need to

1. Create an account at [auth0](https://auth0.com/)
2. Create a new Single Page Application client
3. Make sure `http://localhost:3000` is set up in Allowed Callback URLs, Allowed Logout URLs and Allowed Origins (CORS).
4. Create a new rule `Set roles to a user` and add the contents from `{PLANET_INSTALL}resources/assets/app/internals/scripts/auth0/set-roles-to-a-user.js`. Change {YOUR-EMAIL-ADDRESS} to your eg. google-oauth2 connection email


#### Database backup

TBD

#### User-Agent

TBD


### Important directories


#### Back-end

REST API routes

    {PLANET_INSTALL}/routes/web.php

App

    {PLANET_INSTALL}/app

Cron setup

    {PLANET_INSTALL}/app/Console/Kernel.php

#### Front-end

Command scripts

    {PLANET_INSTALL}/resources/assets/internals

Screens and components

    {PLANET_INSTALL}/resources/assets/screens

API, state, AuthService etc.

    {PLANET_INSTALL}/resources/assets/services


### Setting up Cron

Add the following entry in crontab:

    # Run the Laravel scheduler every minute.
    0 * * * *  php /var/www/html/planet-react.org/public_html/artisan schedule:run

See `{PLANET_INSTALL}/app/Console/Kernel.php` for more scheduled jobs.



