<?php

$api = app( 'Dingo\Api\Routing\Router' );

Route::get( '/{any}', function () {
    return view( 'index' );
} )->where( 'any', '.*' );


$api->version( 'v1', [ 'middleware' => [ 'cors', 'locale' ] ], function ( $api ) {
    $api->get( '/', function () {
        return [];
    } );

    // Posts

    $api->get( '/posts', 'App\Api\V1\Controllers\Pub\PostController@index' )->where( [
            'blogId'   => '[\d+,]+',
            'language' => '[\w+]'
    ] );
    $api->get( '/posts/top-posters', 'App\Api\V1\Controllers\Pub\PostController@getTopPosters' );

    // Blogs

    $api->get( '/blogs', 'App\Api\V1\Controllers\Pub\BlogController@index' );
    $api->post( '/blogs', 'App\Api\V1\Controllers\Pub\BlogController@store' );
    $api->get( '/blogs/top-blogs', 'App\Api\V1\Controllers\Pub\BlogController@getTopBlogs' );
    $api->post( '/blogs/blog-info', 'App\Api\V1\Controllers\Pub\BlogController@getBlogInfo' );

    // Admin

    $api->group( [ 'middleware' => [ 'auth0.jwt' ], 'prefix' => '/admin' ], function ( $api ) {

        // Blogs

        $api->get( '/blogs', 'App\Api\V1\Controllers\Admin\AdminBlogController@index' )->where( [
                'notApproved'    => '[\d+,]+',
                'orderBy'        => '[\w+,]+',
                'orderDirection' => '[\w+,]+'
        ] );
        $api->delete( '/blogs/{id}', 'App\Api\V1\Controllers\Admin\AdminBlogController@delete' );
        $api->put( '/blogs/{id}', 'App\Api\V1\Controllers\Admin\AdminBlogController@update' );

        // Stats

        $api->post( '/stats/stats', 'App\Api\V1\Controllers\Admin\AdminStatsController@getStats' );

        // Logs

        $api->post( '/logs/fetch', 'App\Api\V1\Controllers\Admin\AdminLogController@getFetchLog' );

        // Cron

        $api->post( '/cron/info', 'App\Api\V1\Controllers\Admin\AdminCronController@getInfo' );
    } );

} );
