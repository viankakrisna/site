require( 'dotenv' ).config();

const webpack = require( 'webpack' );
const WebpackDevServer = require( 'webpack-dev-server' );
const webpackConfig = require( './webpack.app.development.config' );

const API_PATH = '/' + process.env.API_PREFIX;

new WebpackDevServer( webpack( webpackConfig ), {
    publicPath: webpackConfig.output.publicPath,
    hot: true,
    compress: true,
    historyApiFallback: true,
    proxy: {
        [API_PATH]: {
            target: process.env.PLANET_SERVER_DEVELOPMENT_API_URL,
            secure: false,
            changeOrigin: true
        }
    }
} ).listen( process.env.PLANET_SERVER_DEVELOPMENT_PORT, process.env.PLANET_SERVER_DEVELOPMENT_HOST, function ( err, result ) {
    if ( err ) {
        return console.log( err );
    }
    console.log( 'Listening at ' + process.env.PLANET_SERVER_DEVELOPMENT_URL + '/' );
    console.log( 'Proxy ' + process.env.PLANET_SERVER_DEVELOPMENT_API_URL + '/' );
} );