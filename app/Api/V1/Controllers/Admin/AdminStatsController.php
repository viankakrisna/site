<?php

namespace App\Api\V1\Controllers\Admin;

use App\Blog;
use App\Post;
use App\Api\V1\Controllers\BaseController;
use App\Api\V1\Scopes\BlogApprovedScope;
use Illuminate\Support\Facades\DB;

class AdminStatsController extends BaseController {

    public function getStats() {

        $blogs                   = Blog::withoutGlobalScope( BlogApprovedScope::class )->count();
        $blogsWaitingForApproval = Blog::withoutGlobalScope( BlogApprovedScope::class )->where( 'approved', 0 )->count();
        $posts                   = Post::count();
        $postsFetchedThisMonth   = DB::select( DB::raw( "
                              SELECT DATE_FORMAT(created_at,'%e. %b') AS `day`, COUNT(id) AS `count` 
                              FROM posts 
                              WHERE MONTH(created_at) = MONTH(NOW()) 
                              GROUP BY `day` 
                              ORDER BY `day` ASC" ) );
        $failedBlogs             = Blog::withoutGlobalScope( BlogApprovedScope::class )->where( 'failed', 1 )->count();

        $stats = [
                'data' => [
                        'blogs'                      => $blogs,
                        'blogs_waiting_for_approval' => $blogsWaitingForApproval,
                        'failed_blogs'               => $failedBlogs,
                        'posts'                      => $posts,
                        'posts_fetched_this_month'   => $postsFetchedThisMonth
                ]
        ];

        return $this->response->array( $stats )->setStatusCode( 201 );
    }

}
