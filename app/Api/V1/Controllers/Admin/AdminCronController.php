<?php

namespace App\Api\V1\Controllers\Admin;

use App;
use App\Api\V1\Controllers\BaseController;
use Artisan;
use Carbon\Carbon;
use Cron\CronExpression;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\File;

class AdminCronController extends BaseController {

    public function getInfo() {

        Artisan::call( 'schedule:list' );
        $json = json_decode( Artisan::output() );

        $sc = null;
        foreach ( $json->events as $schedule ) {
            if ( str_contains( $schedule->command, 'planet:fetch' ) ) {
                $sc = $schedule;
                break;
            }
        }

        if ( ! $sc ) {
            return $this->response->noContent();
        }

        $data = [
                'data' => [
                        'next_schedule'         => CronExpression::factory( $sc->cron )->getNextRunDate()->format( 'Y-m-d H:i:s' ),
                        'job'              => $sc->command,
                        'lock_file_status' => $this->getLockFileStatus()
                ]
        ];

        return $this->response->array( $data )->setStatusCode( 201 );
    }

    private function getLockFileStatus() {
        $path = storage_path( 'app' ) . '/.fetch.lock';
        return [
                'file_exists'   => File::exists( $path ),
                'last_modified' => File::exists( $path ) ? Carbon::createFromTimestamp( File::lastModified( $path ) )->toDateTimeString() : false
        ];
    }

}
