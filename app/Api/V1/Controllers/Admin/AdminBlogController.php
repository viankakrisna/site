<?php

namespace App\Api\V1\Controllers\Admin;

use App\Api\V1\Controllers\BaseController;
use App\Api\V1\Scopes\BlogApprovedScope;
use App\Api\V1\Transformers\BlogTransformer;
use App\Blog;
use Dingo\Api\Exception\StoreResourceFailedException;

class AdminBlogController extends BaseController {

    public function index() {
        $notApproved    = request()->input( 'notApproved' );
        $orderBy        = request()->input( 'orderBy' );
        $orderDirection = request()->input( 'orderDirection' ) ? request()->input( 'orderDirection' ) : 'asc';

        $blogs = Blog::withoutGlobalScope( BlogApprovedScope::class );
        $blogs->withCount( 'post' );
        if ( $notApproved == 1 ) {
            $blogs->where( 'approved', 0 );
        }
        if ( $orderBy ) {
            $blogs->orderBy( $orderBy, $orderDirection );
        }
        return $this->response->collection( $blogs->get(), new BlogTransformer );
    }

    public function update( $id ) {
        $input          = json_decode( request()->getContent() );
        $blog           = Blog::withoutGlobalScope( BlogApprovedScope::class )->find( $id );
        $blog->approved = $input->approved;

        $saved = $blog->save();
        if ( ! $saved ) {
            throw new StoreResourceFailedException( 'Failed updating blog' );
        }

        return $this->response->item( $blog, new BlogTransformer )->setStatusCode( 200 );
    }

    public function delete( $id ) {
        $blog = Blog::withoutGlobalScope( BlogApprovedScope::class )->find( $id );
        if ( $blog ) {
            try {
                $blog->delete();
            } catch ( \Exception $e ) {
                throw new \Exception( $e->getMessage() );
            }
        }
        return $this->response->array( [ 'deleted' => true ] )->setStatusCode( 200 );
    }
}
