<?php

namespace App\Api\V1\Controllers\Admin;

use App\Api\V1\Controllers\BaseController;
use App\Helpers\FileHelper;
use Dingo\Api\Contract\Http\Request;
use Illuminate\Support\Facades\File;

class AdminLogController extends BaseController {

    public function getFetchLog( Request $request ) {
        $input = json_decode( $request->getContent() );
        $date  = property_exists( $input, 'date' ) ? $input->date : date( "Y-m-d" );

        // @todo: centralize fetch log location.
        $fetchLogLocation = storage_path( 'app' ) . "/fetch-{$date}.log";

        if ( ! File::exists( $fetchLogLocation ) ) {
            return $this->response->noContent();
        }

        $data = [
                'data' => [
                        'log' => File::get( $fetchLogLocation )
                ]
        ];

        return $this->response->array( $data )->setStatusCode( 201 );
    }

}
