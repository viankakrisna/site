<?php

namespace App\Api\V1\Controllers\Pub;

use App\Api\V1\Controllers\BaseController;
use App\Api\V1\Transformers\BlogTransformer;
use App\Api\V1\Transformers\SimpleBlogTransformer;
use App\Blog;
use Dingo\Api\Contract\Http\Request;
use Dingo\Api\Exception\StoreResourceFailedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BlogController extends BaseController {

    public function index() {
        $blogs = Blog::orderBy( 'name', 'asc' );
        return $this->response->collection( $blogs->get(), new BlogTransformer() );
    }

    public function getTopBlogs() {
        $blogs = Blog::withCount( [ 'post' => function ( $query ) {
            $query->whereYear( 'pubdate', '=', date( 'Y' ) )
                    ->whereMonth( 'pubdate', '=', date( 'm' ) );
        } ] )->get()->sortBy( function ( Blog $blog ) {
            return $blog->post_count;
        }, SORT_REGULAR, true )->filter( function ( $blog ) {
            return $blog->post_count > 0;
        } )->take( 10 );

        return $this->response->collection( $blogs, new BlogTransformer );
    }

    public function store( Request $request ) {
        $input = json_decode( $request->getContent() );

        $blog       = new Blog();
        $blog->name = trim( $input->name );
        $blog->lang = $input->lang;
        $blog->url  = trim( $input->url );
        if ( $input->is_rss == 1 ) {
            $blog->is_rss   = $input->is_rss;
            $blog->feed_url = trim( $input->feed_url );
        }

        if ( ! $blog->validate() ) {
            throw new StoreResourceFailedException( 'Validation error', $blog->errors() );
        }

        if ( $blog->exists() ) {
            throw new StoreResourceFailedException( "Hey! Looks like this feed is already is added" );
        }

        $blog->save();

        return $this->response->item( $blog, new BlogTransformer )->setStatusCode( 201 );
    }

    public function getBlogInfo( Request $request ) {
        $input = json_decode( $request->getContent() );

        $blog = Blog::find( $input->id );

        if ( ! $blog ) {
            throw new NotFoundHttpException( 'Invalid id' );
        }

        return $this->response->item( $blog, new SimpleBlogTransformer )->setStatusCode( 201 );
    }

}
