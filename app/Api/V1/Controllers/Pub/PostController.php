<?php

namespace App\Api\V1\Controllers\Pub;

use App\Api\V1\Controllers\BaseController;
use App\Api\V1\Transformers\PostTransformer;
use App\Post;

class PostController extends BaseController {

    public function index() {

        $blogId   = request()->input( 'blogId' );
        $language = request()->input( 'language' );

        $posts = Post::whereHas('blog')->whereYear('pubdate', '=',date('Y'))->whereMonth( 'pubdate', '=', date( 'm' ) );
        if ( $blogId ) {
            $posts->where( 'blog_id', $blogId );
        }

        if ( $language ) {
            $posts->whereHas( 'blog', function ( $query ) use ( $language ) {
                $query->where( 'lang', $language );
            } );
        }

        $posts->orderBy( 'pubdate', 'desc' );

        $posts = $posts->get();

        return $this->response->collection( $posts, new PostTransformer );
    }

    public function getTopPosters() {

        $posts = Post::select( 'blog_id', 'authors', 'url as blog_url', \DB::raw( 'count(*) as total_posts' ) )
                ->leftJoin( 'blogs', 'posts.blog_id', '=', 'blogs.id' )
                ->where( 'authors', '<>', '' )
                ->whereYear( 'pubdate', '=', date( 'Y' ) )
                ->whereMonth( 'pubdate', '=', date( 'm' ) )
                ->groupBy( 'blog_id','authors' )
                ->orderBy( 'total_posts', 'desc' )
                ->having( 'total_posts', '>', 0 )
                ->limit( 10 )
                ->get();

        return [ 'data' => $posts ];
    }

}
