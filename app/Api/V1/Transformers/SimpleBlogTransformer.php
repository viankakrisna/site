<?php
namespace App\Api\V1\Transformers;

use App\Blog;
use League\Fractal;

class SimpleBlogTransformer extends Fractal\TransformerAbstract {
    public function transform( Blog $blog ) {
        return [
                'id'         => (int)$blog->id,
                'name'       => $blog->name ? $blog->name : '',
                'url'        => $blog->url ? $blog->url : '',
                'feed_url'   => $blog->feed_url ? $blog->feed_url : ''
        ];
    }
}