<?php
namespace App\Api\V1\Transformers;

use App\Post;
use Carbon\Carbon;
use League\Fractal;

class PostTransformer extends Fractal\TransformerAbstract {
    public function transform( Post $post ) {
        return [
                'id'                 => (int)$post->id,
                'title'              => $post->title ? $post->title : '',
                'authors'            => $post->authors ? $post->authors : '',
                'description'        => str_limit( $post->description ? $post->description : '', 200 ),
                'pubdate'            => $post->pubdate ? $post->pubdate : '',
                'pubdate_for_humans' => $post->pubdate ? Carbon::createFromTimestamp( strtotime( $post->pubdate ) )->diffForHumans() : '',
                'link'               => $post->link ? $post->link : '',
                'blog_name'          => $post->blog->name,
                'blog_url'           => $post->blog->url,
                'feed_url'           => $post->blog->feed_url
        ];
    }
}
