<?php
namespace App\Api\V1\Transformers;

use App\Blog;
use League\Fractal;

class BlogTransformer extends Fractal\TransformerAbstract {
    public function transform( Blog $blog ) {
        return [
                'id'         => (int)$blog->id,
                'name'       => $blog->name ? $blog->name : '',
                'url'        => $blog->url ? $blog->url : '',
                'feed_url'   => $blog->feed_url ? $blog->feed_url : '',
                'lang'       => $blog->lang ? $blog->lang : '',
                'approved'   => $blog->approved,
                'is_rss'     => $blog->is_rss,
                'failed'     => $blog->failed,
                'post_count' => $blog->post_count ? $blog->post_count : 0,
        ];
    }
}