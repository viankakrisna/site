<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

class BaseModel extends Model {
    use SoftDeletes;

    protected $rules = [];

    protected $errors;

    public function validate() {
        $v = Validator::make( $this->toArray(), $this->rules );
        if ( $v->fails() ) {
            $this->errors = $v->errors();
            return false;
        }
        return true;
    }

    public function errors() {
        return $this->errors;
    }

}
