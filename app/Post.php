<?php

namespace App;

class Post extends BaseModel {

    public function blog() {
        return $this->belongsTo( 'App\Blog' );
    }

    public function exists() {
        return Post::where( 'link', 'like', '%' . rtrim( $this->link, '/' ) . '%' )->count() > 0;
    }
}
