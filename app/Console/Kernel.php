<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\AggregateCommand::class,
        Commands\DatabaseBackupCommand::class,
        Commands\ScheduleList::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $backupTime = env( 'PLANET_BACKUP_EVERY_DAY_AT' )
                ? env( 'PLANET_BACKUP_EVERY_DAY_AT' )
                : "06:00";

        $schedule->command( 'planet:aggregate' )->hourly();
        $schedule->command( 'planet:db-backup' )->dailyAt( $backupTime );
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
