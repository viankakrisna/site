<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Storage;

class DatabaseBackupCommand extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'planet:db-backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backs up the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        if ( ! env( 'PLANET_BACKUP' ) ) {
            $this->info( "PLANET_BACKUP is not set or `false`" );
            return;
        }

        $host = parse_url( env( 'APP_URL' ) )['host'];
        $fileName  = "{$host}-dump.sql";

        $directory = storage_path( 'app' ) . '/db-backup';
        $localPath = $directory . '/' . $fileName;

        if ( ! File::isDirectory( $directory ) ) {
            $this->info( "Creating directory {$directory}" );
            File::makeDirectory( $directory );
        }

        $this->info( "Creating backup {$localPath}" );

        $username = escapeshellarg( env( 'DB_USERNAME' ) );
        $password = escapeshellarg( env( 'DB_PASSWORD' ) );
        exec( "mysqldump -u {$username} -p{$password} planet_react > {$localPath}" );

        $this->info( "Saving to Google Drive" );
        Storage::disk( 'google' )->put( 'backup.sql', file_get_contents( $localPath ) );
        $this->info( "Done" );

    }

}
