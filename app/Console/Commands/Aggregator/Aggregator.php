<?php
namespace App\Console\Commands\Aggregator;

use App\Console\Commands\Aggregator\Handlers\Scraper;
use App\Console\Commands\Aggregator\Handlers\Rss;
use App\Blog;
use App\Post;
use Illuminate\Support\Facades\File;

class Aggregator {
    protected $lockFile;
    protected $blogs = [];
    protected $blogCount = 0;
    protected $numberOfBlogPostsToStorePerFetch = 5;
    protected $progressCount = 0;
    protected $blogFetchFails = [];
    protected $totalExecutionTime = 0;
    protected $postsSaved = 0;
    protected $logger;

    public function __construct( $logger, $blogId = false ) {
        $this->lockFile  = storage_path( 'app' ) . '/.fetch.lock';
        $this->blogs     = $blogId ? Blog::find( [ $blogId ] ) : Blog::all();
        $this->blogCount = $this->blogs->count();
        $this->logger    = $logger;
    }

    public function run() {
        if ( $this->isLocked() ) {
            $this->log( 'Lock file exists' );
            return false;
        }
        $this->createLock();

        foreach ( $this->blogs as $blog ) {
            $this->progressCount++;

            $timeStart = microtime( true );

            $failed = false;

            $count = 0;

            $this->log( "Fetching blog {$this->progressCount}/{$this->blogCount}: #{$blog->id} {$blog->name}, RSS Feed: {$blog->hasFeed()}" );
            try {
                if ( $blog->is_rss ) {
                    $posts = Rss::fetch( $blog->feed_url );
                }
                else {
                    $blogScraper = new Scraper( $blog );
                    $posts       = $blogScraper->scrape();
                }
                $count = $this->storeNewBlogPosts( $posts, $blog->id );

            } catch ( \Exception $exception ) {
                $failed = true;
                $this->log( "Failed storing posts from {$blog->name} - {$exception->getTraceAsString()}", true );
                $this->blogFetchFails[] = $blog->id;
            }

            $timeEnd       = microtime( true );
            $executionTime = ( $timeEnd - $timeStart );

            $blog->failed          = $failed ? 1 : 0;
            $blog->last_fetched_at = date( 'Y-m-d H:i:s' );
            $blog->execution_time  = $executionTime;
            $blog->save();

            $this->log( "Posts saved: {$count}, Time: {$executionTime} sec" );
            if ( $this->progressCount < $this->blogCount ) {
                $this->log( "------------------------------------------------" );
            }

            $this->totalExecutionTime += $executionTime;

            // Make it less aggressive.
            sleep( 2 );
        }

        $this->removeLock();

        return [
                'posts_saved'          => (int)$this->postsSaved,
                'fetch_failed'         => $this->blogFetchFails,
                'total_execution_time' => $this->totalExecutionTime,
        ];
    }

    private function storeNewBlogPosts( $postData, $blogId ) {
        $count = 0;
        for ( $i = 0; $i < $this->numberOfBlogPostsToStorePerFetch; $i++ ) {
            if ( empty( $postData[$i] ) ) {
                continue;
            }

            // Avoid duplicate links.
            // @todo: use guid
            $post = Post::where( 'link', '=', $postData[$i]->link );
            if ( $post->count() == 0 ) {
                $post              = new Post();
                $post->blog_id     = $blogId;
                $post->pubdate     = $postData[$i]->pubdate;
                $post->title       = $postData[$i]->title;
                $post->description = $postData[$i]->description ? $postData[$i]->description : '';
                $post->link        = $postData[$i]->link;
                $post->guid        = $postData[$i]->guid;
                $post->fetched_at  = date( 'Y-m-d H:i:s' );
                if ( property_exists( $postData[$i], 'authors' ) ) {
                    $post->authors = $postData[$i]->authors;
                }

                // Avoid posts in with future dates.
                $pubDate = new \DateTime( $post->pubdate );
                $now     = new \DateTime();
                if ( $pubDate > $now ) {
                    continue;
                }

                if ( ! $post->exists() ) {
                    $post->save();
                    $count++;
                    $this->postsSaved++;
                }
            }
        }

        return $count;
    }

    private function isLocked() {
        return File::exists( $this->lockFile );
    }

    private function createLock() {
        File::put( $this->lockFile, '1' );
    }

    private function removeLock() {
        if ( File::exists( $this->lockFile ) ) {
            File::delete( $this->lockFile );
        }
    }

    private function log( $msg, $write = false ) {
        call_user_func( $this->logger, $msg, $write );
    }

}
