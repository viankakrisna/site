<?php

namespace App\Console\Commands\Aggregator\Handlers;

use SimplePie;

class Rss {

    const FEED_SOURCES_THAT_SHOULD_BE_FILTERED_BY_CATEGORY = [
            'medium.com',
            'dzone.com'
    ];

    const REACT_CATEGORIES = [
            'react', 'reactjs', 'react-js', 'react_js'
    ];

    static public function fetch( $url ) {
        $feed = new SimplePie();
        $feed->set_timeout( env( 'PLANET_FETCHER_TIMEOUT', 15 ) );
        $feed->set_useragent( env( 'PLANET_FETCHER_USER_AGENT' ) );
        $feed->set_feed_url( $url );
        $feed->enable_cache( false );
        $feed->init();
        $feed->handle_content_type();
        $result = [];

        foreach ( $feed->get_items() as $item ) {
            $authors = $item->get_authors();

            $data              = new \stdClass;
            $data->title       = $item->get_title();
            $data->description = strip_tags( $item->get_description() );
            $data->pubdate     = $item->get_date( 'Y-m-d H:i:s' );
            $data->link        = $item->get_link();
            $data->guid        = $item->get_id();

            // Temporary solution for Dan Abramov.
            if ( $url != 'https://medium.com/feed/@dan_abramov' ) {
                if ( self::feedShouldBeFilteredByCategory( $feed ) ) {
                    if ( ! self::postHasReactCategory( $item ) ) {
                        continue;
                    }
                }
            }

            if ( ! empty( $authors ) ) {
                $data->authors = implode( ", ", array_map( function ( $author ) {
                    return $author->name;
                }, $authors ) );
            }
            else {
                $data->authors = '';
            }

            $result[] = $data;
        }

        return $result;
    }

    /**
     * @param SimplePie $feed
     * @return boolean
     */
    static private function feedShouldBeFilteredByCategory( $feed ) {
        $channelLink = $feed->get_link();
        if ( ! $channelLink ) {
            return false;
        }
        foreach ( self::FEED_SOURCES_THAT_SHOULD_BE_FILTERED_BY_CATEGORY as $channel ) {
            if ( str_contains( $channelLink, $channel ) ) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param \SimplePie_Item $item
     * @return boolean
     */
    static private function postHasReactCategory( $item ) {
        $categories = $item->get_categories();
        if ( ! $categories ) {
            return false;
        }

        foreach ( $categories as $category ) {
            if ( in_array( strtolower( $category->term ), self::REACT_CATEGORIES ) ) {
                return true;
            }
        }
        return false;
    }

}
