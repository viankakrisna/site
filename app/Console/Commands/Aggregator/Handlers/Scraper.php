<?php

namespace App\Console\Commands\Aggregator\Handlers;

use App\Blog;
use Carbon\Carbon;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

class Scraper {
    protected $blogModel;
    protected $client;

    public function __construct( Blog $blogModel ) {
        $this->blogModel = $blogModel;
        $this->client    = new Client();
        $this->client->setClient( new GuzzleClient( [
                'timeout' => env( 'PLANET_FETCHER_TIMEOUT', 15 )
        ] ) );

        $this->client->setHeader( 'user-agent', env( 'PLANET_FETCHER_USER_AGENT' ) );
    }

    public function scrape() {
        $url                 = $this->blogModel->feed_url;
        $postsPath           = $this->blogModel->posts_xpath;
        $postTitlePath       = $this->blogModel->post_title_xpath;
        $postDescriptionPath = $this->blogModel->post_description_xpath;
        $postPubDatePath     = $this->blogModel->post_pubdate_xpath;
        $postLinkPath        = $this->blogModel->post_link_xpath;

        $crawler = $this->client->request( 'GET', $url );

        $result = [];
        $crawler->filter( $postsPath )->each( function ( $node ) use ( &$result, $url, $postTitlePath, $postDescriptionPath, $postPubDatePath, $postLinkPath ) {

            try {
                $data          = new \stdClass;
                $data->title   = $node->filter( $postTitlePath )->text();
                $data->pubdate = Carbon::parse( $this->parseValue( $node, $postPubDatePath ) )->format( 'Y-m-d H:i:s' );
                $data->link    = $this->parseLink( $node->filter( $postLinkPath )->attr( 'href' ) );
                $data->guid    = md5( $this->parseLink( $node->filter( $postLinkPath )->attr( 'href' ) ) );
//                $data->description = $postDescriptionPath ? strip_tags( $node->filter( $postDescriptionPath )->text() ) : '';
                $data->description = '';
            } catch ( \Exception $exception ) {

            }

            $result[] = $data;
        } );

        return $result;
    }

    protected function parseLink( $link ) {
        $isAbsolute = starts_with( $link, [ 'http', 'https' ] );
        if ( $isAbsolute ) {
            return $link;
        }

        $url = $this->blogModel->url;
        if ( ends_with( $url, '/' ) ) {
            $url = rtrim( $url, '/' );
        }

        if ( starts_with( $link, '/' ) ) {
            $link = ltrim( $link, '/' );
        }
        return $url . '/' . $link;
    }

    private function parseValue( $node, $selector ) {
        $attrPattern     = "/\[(.+)\]/";
        $hasAttrSelector = preg_match( $attrPattern, $selector, $matches );
        if ( $hasAttrSelector ) {
            $selectorWithoutAttr = preg_replace( $attrPattern, "", $selector );
            $result              = $node->filter( $selectorWithoutAttr )->attr( $matches[1] );
        }
        else {
            $result = $node->filter( $selector )->text();
        }
        return $result;
    }

}
