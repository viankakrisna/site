<?php

namespace App\Console\Commands;

use App\Console\Commands\Aggregator\Aggregator;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class AggregateCommand extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'planet:aggregate {blogId?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Aggregates posts from feeds';

    /**
     * Path to fetch log file.
     *
     * @var null|string
     */
    protected $fetchLogLocation = null;

    /**
     * Create a new command instance.
     */
    public function __construct() {
        parent::__construct();

        $this->fetchLogLocation = storage_path( 'app' ) . "/fetch-".date("Y-m-d").".log";
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $blogId = $this->argument( 'blogId' );

        $logger = function ( $msg, $write = false ) {
            $message = gettype( $msg ) == 'array' || gettype( $msg ) == 'object' ? json_encode( $msg ) : $msg;
            if ( $write ) {
                logger( $message );
            }
            $d   = date( "Y-m-d H:i:s" );
            $out = "[{$d}] {$message}";
            $this->info( $out );

            File::append( $this->fetchLogLocation, $out . "\n" );
        };

        $aggregator = new Aggregator( $logger, $blogId );
        $result  = $aggregator->run();
        if ( ! $result ) {
            return;
        }

        $blogsFailed = count( $result['fetch_failed'] );

        $out     = "Saved: {$result['posts_saved']}, Failed: {$blogsFailed}, Total Time: {$result['total_execution_time']}";
        $divider = str_repeat( "*", strlen( $out ) );

        $logger( $divider );
        $logger( $out );
        $logger( $divider );

    }

}