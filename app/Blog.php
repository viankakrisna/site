<?php

namespace App;

use App\Api\V1\Scopes\BlogApprovedScope;

class Blog extends BaseModel {
    protected $rules = [
            'name'     => 'required|min:2',
            'url'      => 'required|url',
            'feed_url' => 'sometimes|required|url',
    ];

    protected static function boot() {
        parent::boot();
        static::addGlobalScope( new BlogApprovedScope );
    }

    public function post() {
        return $this->hasMany( 'App\Post' );
    }

    public function hasFeed() {
        return $this->is_rss ? 'Yes' : 'No';
    }

    public function exists() {
        if ( $this->is_rss == 1 ) {
            $column = 'feed_url';
            $url    = $this->feed_url;
        }
        else {
            $column = 'url';
            $url    = $this->url;
        }

        return Blog::where( $column, 'like', '%' . rtrim( $url, '/' ) . '%' )->withoutGlobalScopes()->count() > 0;
    }

}
