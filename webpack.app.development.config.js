require( 'dotenv' ).config();
const path = require( 'path' );
const webpack = require( 'webpack' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const precss = require( 'precss' );
const autoprefixer = require( 'autoprefixer' );
const packageJson = require( './package.json' );

/**
 * Webpack configuration for development.
 * See .env file for constants.
 *
 * https://webpack.js.org/
 */
module.exports = {
    /**
     * Cache generated modules and chunks
     */
    cache: true,
    /**
     * webpack creates a graph of all of your application's dependencies.
     * The starting point of this graph is known as an entry point.
     * The entry point tells webpack where to start and follows the graph of
     * dependencies to know what to bundle. You can think of your application's
     * entry point as the contextual root or the first file to kick off your app.
     *
     * https://webpack.js.org/concepts/#entry
     */
    entry: [
        /**
         * Include webpack dev server resources.
         */
        'react-hot-loader/patch',
        'babel-polyfill',
        'webpack-dev-server/client?' + process.env.PLANET_SERVER_DEVELOPMENT_URL,
        'webpack/hot/only-dev-server',

        /**
         * Path to our app's entry point.
         */
        path.resolve( __dirname, process.env.PLANET_APP_ENTRY )
    ],

    /**
     * Once you've bundled all of your assets together, we still need to tell
     * webpack where to bundle our application. The webpack output property
     * describes to webpack how to treat bundled code.
     *
     * https://webpack.js.org/concepts/#output
     */
    output: {
        /**
         * The output directory as an absolute path.
         *
         * https://webpack.js.org/configuration/output/#output-path
         */
        path: path.resolve( __dirname, process.env.PLANET_JS_OUTPUT ),

        /**
         * publicPath specifies the public URL of the output directory when
         * referenced in a browser.
         *
         * https://webpack.js.org/configuration/output/#output-publicpath
         */
        publicPath: '/',

        /**
         * This option determines the name of each output bundle.
         * The bundle is written to the directory specified by the output.path option.
         *
         * https://webpack.js.org/configuration/output/#output-filename
         */
        filename: 'app.[hash].js'
    },

    /**
     * Turn of performance hints in development mode as assets are not minimized.
     *
     * https://webpack.js.org/configuration/performance/
     */
    performance: {
        hints: false
    },

    /**
     * This option controls if and how Source Maps are generated.
     *
     * https://webpack.js.org/configuration/devtool/
     */
    devtool: 'eval',

    /**
     * Options in .module determine how the different types of modules within a
     * project will be treated during creation.
     *
     * https://webpack.js.org/concepts/#loaders
     */
    module: {
        /**
         * An array of Rules which are matched to requests when modules are created.
         *
         * https://webpack.js.org/configuration/module/#module-rules
         */
        rules: [
            /**
             * JavaScript
             *
             * This rule transpiles ES6 and ES7 code to JavaScript
             * see also /.babelrc for options.
             *
             * https://github.com/babel/babel-loader
             */
            {
                test: /\.js?$/,
                include: path.resolve( __dirname, process.env.PLANET_APP_ENTRY ),
                exclude: /node_modules/,
                loader: 'babel-loader?cacheDirectory=true'
            },

            /**
             * CSS
             *
             * Adds CSS to the DOM by injecting a <style> tag.
             *
             * https://github.com/webpack/style-loader
             */
            {
                test: /\.scss$/,
                loader: 'style-loader!css-loader!postcss-loader!resolve-url-loader!sass-loader?sourceMap'
            },

            /**
             * Fonts
             *
             * Creates a Data Url of font files when module is created.
             */
            {
                test: /\.(woff|woff2|eot|ttf)$/,
                loader: 'url-loader?limit=100000'
            },

            /**
             * Image files
             *
             * Creates a Data Url of image files when module is created.
             */
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'file-loader?hash=sha512&digest=hex&name=app/[hash].[ext]',
                    'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            },

            /**
             * Json
             */
            {
                test: /\.json$/, loader: 'json'
            }
        ]
    },

    /**
     * Since rules only execute transforms on a per-file basis, plugins are most
     * commonly used (but not limited to) performing actions and custom
     * functionality on "compilations" or "chunks" of your bundled modules
     * (and so much more).
     *
     * https://webpack.js.org/concepts/#plugins
     */
    plugins: [

        /**
         * Injects variables into the application.
         */
        new webpack.DefinePlugin( {
            '__APP_VERSION__': JSON.stringify( packageJson.version ),
            '__APP_USER_AGENT__': JSON.stringify( process.env.PLANET_FETCHER_USER_AGENT ),
            '__APP_REPO__': JSON.stringify( packageJson.repository.url ),
            '__AUTH_DOMAIN__': JSON.stringify( process.env.PLANET_AUTH0_DOMAIN ),
            '__AUTH_CLIENT_ID__': JSON.stringify( process.env.PLANET_AUTH0_CLIENT_ID ),
            '__AUTH_CLIENT_REDIRECT_URL__': JSON.stringify( process.env.PLANET_AUTH0_REDIRECT_URL ),
            'process.env': {
                NODE_ENV: JSON.stringify( 'development' )
            }
        } ),

        /**
         * Prints more meaningful module names in the browser console on updates.
         */
        new webpack.NamedModulesPlugin(),

        /**
         * Hot Module Reloading (HMR).
         */
        new webpack.HotModuleReplacementPlugin(),

        /**
         * Create HTML template
         */
        new HtmlWebpackPlugin( {
            title: '',
            hash: false,
            inject: true,
            template: process.env.PLANET_HTML_TEMPLATE
        } ),

        /**
         * TBD
         */
        new webpack.LoaderOptionsPlugin( {
            test: /\.scss$/,
            debug: true,
            options: {
                postcss: function () {
                    return [ precss, autoprefixer ];
                },
                context: path.resolve( __dirname, process.env.PLANET_APP_ENTRY ),
                output: {
                    path: path.resolve( __dirname, process.env.PLANET_JS_OUTPUT )
                }
            }
        } )
    ],
};
