import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import DocumentTitle from '../../../../shared/components/support/DocumentTitle';
import Rpc from '../../../../shared/components/support/rest/Rpc';

@inject( 'appState' )
@inject( 'collections' )
@observer
export default class Log extends Component {

    state = {
        inputDate: this.dateTodayFormatted(),
        date: this.dateTodayFormatted()
    };

    refreshInterval = -1;

    constructor( props ) {
        super( props );
        this.appState = props.appState;
        this.collections = props.collections;
    }

    componentDidMount() {
        this.refreshInterval = setInterval( () => {
            this.fetchInfo();
        }, 60000 );
    }

    componentWillUnmount() {
        clearInterval( this.refreshInterval );
    }

    dateTodayFormatted() {
        const d = new Date();
        let mm = d.getMonth() + 1;
        let dd = d.getDate();
        const yyyy = d.getFullYear();
        mm = mm < 10 ? '0' + mm : mm;
        dd = dd < 10 ? '0' + dd : dd;
        return `${yyyy}-${mm}-${dd}`;
    }

    async fetchInfo() {
        const { adminCronCollection } = this.collections;
        this.appState.pendingRequests++;
        await adminCronCollection.rpc( 'info' );
        this.appState.pendingRequests--;
    }

    onChangeDate = ( event ) => {
        const { target } = event;
        this.setState( {
            inputDate: target.value
        } );
    };

    onKeyUp = ( event ) => {
        if ( event.charCode == 13 ) {
            this.setState( {
                date: this.state.inputDate
            } );
        }
    };

    render() {

        const {
            adminCronCollection,
            adminLogCollection
        } = this.collections;

        return (
            <div>
                <DocumentTitle title="Fetcher"/>
                <div style={{ height: '100px', marginBottom:'1em' }}>
                    <h2>Status</h2>
                    <Rpc collection={adminCronCollection}
                         method="info">
                        {( data ) => (
                            <div>
                                <div>
                                    Fetch in
                                    progress?: {data.lock_file_status.file_exists ? 'Yes' : 'No'}
                                    {data.lock_file_status.file_exists &&
                                    <span>, started: {data.lock_file_status.last_modified}</span>
                                    }
                                </div>
                                <div>
                                    <span>Cron: </span>
                                    <code>
                                        {data.job}
                                    </code>
                                </div>
                                <div>Next run date: {data.next_schedule}</div>
                            </div>
                        )}
                    </Rpc>
                </div>

                <hr/>

                <div style={{marginTop: '1em'}}>
                    <h2>Log</h2>
                    <p>
                        <input type="date" value={this.state.inputDate}
                               onChange={this.onChangeDate}
                               onKeyPress={this.onKeyUp}/>
                    </p>

                    <Rpc collection={adminLogCollection}
                         method="fetch" params={{ date: this.state.date }}>
                        {( data ) => (
                            <div style={{ position: 'relative' }}>
                                <pre style={{
                                    position: 'absolute',
                                    left: '0',
                                    right: '0',
                                    padding: '.5em',
                                    overflow: 'scroll',
                                    maxHeight: '700px',
                                    fontSize: '12px',
                                    background: '#eee'
                                }}>
                                    {data.log}
                                </pre>
                            </div>
                        )}
                    </Rpc>
                </div>
            </div>
        )
    }
}