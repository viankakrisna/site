import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import DocumentTitle from '../../../../shared/components/support/DocumentTitle';
import BlogList from './components/BlogList';

@inject( 'appState' )
@inject( 'collections' )
@observer
export default class Log extends Component {

    constructor( props ) {
        super( props );
        this.appState = props.appState;
        this.collections = props.collections;
    }

    render() {
        return (
            <div>
                <DocumentTitle title="Blogs"/>
                <BlogList collection={this.collections.adminBlogCollection} appState={this.appState}/>
            </div>
        )
    }
}