import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import CollectionFetcher from '../../../../../shared/components/support/rest/CollectionFetcher';
import A from '../../../../../shared/components/A';

export default class BlogList extends Component {

    static propTypes = {
        appState: React.PropTypes.any,
        collection: React.PropTypes.any,
    };

    /**
     * @type {AppState}
     */
    appState = null;

    /**
     * @type {AdminBlogCollection}
     */
    adminBlogCollection = null;

    fetchParams = {
        orderBy: 'approved'
    };

    constructor( props ) {
        super( props );
        this.appState = props.appState;
        this.adminBlogCollection = props.collection;
    }

    onApprove = ( model, event ) => {
        const val = event.target.checked ? 1 : 0;
        model.set( { approved: val } );
        model.save( model.attributes.toJS(), { patch: false } );
    };

    onDelete = ( model, event ) => {
        let confirm = window.confirm( `Danger! Are you sure you want to delete the blog '${model.get( 'name' )}'` );
        if ( confirm ) {
            model.destroy();
        }
    };

    onRefresh = () => {
        const params = this.fetchParams;
        this.adminBlogCollection.fetch( { data: { params } } );
    };

    render() {
        return (
            <div>
                <p>
                    &#123; <A onClick={this.appState.openSubmitBlogModal}>Add
                    Blog</A> &#125;

                    &#123; <A onClick={this.onRefresh}>
                    Refresh</A> &#125;
                </p>
                <table className="blog-table">
                    <thead>
                    <tr>
                        <th className="align-center">#</th>
                        <th style={{width: '40%'}}>Name</th>
                        <th className="align-center">Feed</th>
                        <th className="align-center">Lang</th>
                        <th className="align-center">Approved</th>
                        <th className="align-center">Posts</th>
                        <th className="align-center"></th>
                    </tr>
                    </thead>
                    <CollectionFetcher
                        collection={this.adminBlogCollection}
                        params={this.fetchParams}
                        tag="tbody">
                        { ( model ) => (
                            <tr key={model.id}
                                className={'row ' + (model.get( 'failed' ) == 1 ? 'failed-row' : '')}>

                                <td className="align-center">
                                    <Link to={`/blogs/${model.id}`}>
                                        {model.id}
                                    </Link>
                                </td>
                                <td>
                                    <Link to={model.get( 'url' )}
                                          title={model.get( 'name' )}>
                                        {model.get( 'name' )}
                                    </Link>
                                </td>
                                <td className="align-center" style={{fontSize:'1.2rem'}}>
                                    {model.get('is_rss') == 1 &&
                                    <Link to={model.get( 'feed_url' )}
                                          title={model.get( 'feed_url' )}>
                                        ➚
                                    </Link>}
                                </td>
                                <td className="align-center">
                                    {model.get( 'lang' )}
                                </td>
                                <td className="align-center">
                                    <input type="checkbox"
                                           checked={model.get( 'approved' ) == 1}
                                           onChange={this.onApprove.bind( this, model )}/>
                                </td>
                                <td className="align-center">
                                    {model.get( 'post_count' )}
                                </td>

                                <td className="align-center">
                                    <A onClick={this.onDelete.bind( this, model )}>Delete</A>
                                </td>
                            </tr>
                        )}
                    </CollectionFetcher>
                </table>
            </div>

        )
    }
}
