import React from 'react';
import { toJS } from 'mobx';
import { VictoryChart, VictoryAxis, VictoryLine, VictoryTheme } from 'victory';

const PostsGraph = ( props ) => {
    const { data } = props;
    const posts = toJS( data.posts_fetched_this_month );
    const height = 120;
    const fontSize = 9;
    return (
        <div>
            <VictoryChart
                theme={VictoryTheme.material}
                height={height}
                padding={{ top: 10, right: 30, bottom: 30, left: 30 }}>
                <VictoryAxis
                    tickValues={posts.map( ( p ) => p.day )}
                    style={{
                        tickLabels: { fontSize: fontSize }
                    }}/>
                <VictoryAxis
                    dependentAxis={true}
                    style={{
                        tickLabels: { fontSize: fontSize }
                    }}/>
                <VictoryLine
                    data={posts}
                    x="day"
                    y="count"
                />
            </VictoryChart>
        </div>
    )
};

PostsGraph.propTypes = {
    data: React.PropTypes.any
};

export default PostsGraph;
