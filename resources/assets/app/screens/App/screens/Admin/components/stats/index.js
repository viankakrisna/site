import React from 'react';
import Rpc from '../../../../shared/components/support/rest/Rpc';
import Numbers from './Numbers';
import PostsGraph from './PostsGraph';

const Stats = ( props ) => {
    const { data } = props;
    return (
        <Rpc collection={props.collection} method="stats">
            {( data ) => (
                <div>
                    <Numbers data={data}/>
                    <h4>Posts fetched this month</h4>
                    <PostsGraph data={data}/>
                </div>
            )}
        </Rpc>
    )
};

Stats.propTypes = {
    collection: React.PropTypes.any
};

export default Stats;
