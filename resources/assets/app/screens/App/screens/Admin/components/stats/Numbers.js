import React from 'react';

const Numbers = ( props ) => {
    const { data } = props;
    return (
        <div className="statistics-container">
            <div className="statistic">
                <div className="value">{data.blogs}</div>
                <div className="label">Blogs</div>
            </div>
            <div className="statistic">
                <div className="value">{data.blogs_waiting_for_approval}</div>
                <div className="label">Waiting for approval</div>
            </div>
            <div className="statistic">
                <div className="value">{data.failed_blogs}</div>
                <div className="label">Failed</div>
            </div>
            <div className="statistic">
                <div className="value">{data.posts}</div>
                <div className="label">posts</div>
            </div>
        </div>
    )
};

Numbers.propTypes = {
    data: React.PropTypes.any
};

export default Numbers;
