import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import DocumentTitle from '../../shared/components/support/DocumentTitle';
import Stats from './components/stats';

@inject( 'appState' )
@inject( 'collections' )
@observer
export default class FAQ extends Component {

    constructor( props ) {
        super( props );
        this.appState = props.appState;
    }

    render() {
        return (
            <div>
                <DocumentTitle title="Admin"/>
                <Stats collection={this.props.collections.adminStatsCollection}/>
            </div>
        )
    }
}