import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import DocumentTitle from '../../../../shared/components/support/DocumentTitle';
import HomeLink from '../../../../shared/components/HomeLink';
import BlogList from '../../../../shared/components/BlogList';

@inject( 'collections' )
@observer
export default class AllBlogs extends Component {

    constructor( props ) {
        super( props );
        this.collections = props.collections;
    }

    render() {

        return (
            <div>
                <DocumentTitle title="All Blogs"/>
                <p>
                    <HomeLink/>
                </p>
                <p>
                    The following list contains all feeds
                    aggregated
                    at this
                    site.
                </p>
                <BlogList
                    blogCollection={this.collections.blogCollection}/>
            </div>
        )
    }
}