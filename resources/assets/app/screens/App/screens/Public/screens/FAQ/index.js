import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import DocumentTitle from '../../../../shared/components/support/DocumentTitle';
import HomeLink from '../../../../shared/components/HomeLink';

@inject( 'appState' )
@observer
export default class FAQ extends Component {

    constructor( props ) {
        super( props );
        this.appState = props.appState;
    }

    render() {

        return (
            <div>
                <DocumentTitle title="FAQ"/>
                <p>
                    <HomeLink/>
                </p>

                <dl>
                    <dt>What is Planet Planet React?</dt>
                    <dd>Planet React is an aggregation of blogs and news from
                        the React community. The views expressed here are those
                        of the author and do not necessarily reflect the views
                        of Facebook.

                    </dd>

                    <dt>Who is behind it?</dt>
                    <dd>The site is maintained by Thomas Andersen. Facebook
                        has nothing to do with it.
                    </dd>

                    <dt>How are blogs selected for Planet React?</dt>
                    <dd>
                        <p>
                            There’s no real policy behind it. If an interesting
                            blog is found or someone submits one, then it will be
                            looked at and if it has unique content or is a well
                            known developer, it will be added.
                        </p>
                        <p>
                            We also usually just take React related content from
                            those blogs. This generally means, that we aggregate
                            the *React* category feed of it, as the usual Planet
                            React
                            reader is not interested in the well-beings of your
                            cat ;) Most weblog software supports that nowadays.
                        </p>
                    </dd>

                    <dt>
                        How often are feeds aggregated?
                    </dt>
                    <dd>
                        Feeds are aggregated every hour
                    </dd>

                    <dt>What User-Agent string is used by the aggregator?</dt>
                    <dd>
                        {__APP_USER_AGENT__}
                    </dd>

                    <dt>
                        How can I add my blog to planet php?
                    </dt>
                    <dd>
                        Please use <a href="#"
                                       onClick={this.appState.openSubmitBlogModal}>this
                        submit form</a>.
                    </dd>

                    <dt>
                        Where is the source code?
                    </dt>
                    <dd>
                        <a href="https://gitlab.com/planet-react/site">https://gitlab.com/planet-react/site</a>
                    </dd>

                    <dt>My blog is on the list, how can I remove it?</dt>
                    <dd>
                        Send an email to {this.appState.contactEmail} and we'll
                        remove it for you
                    </dd>

                </dl>
            </div>
        )
    }
}