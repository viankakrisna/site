import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import PostList from '../../components/PostList';
import DocumentTitle from '../../../../shared/components/support/DocumentTitle';
import Rpc from '../../../../shared/components/support/rest/Rpc';

@inject( 'appState' )
@inject( 'collections' )
@observer
export default class Posts extends Component {

    constructor( props ) {
        super( props );
        this.collections = props.collections;
        this.appState = props.appState;
    }

    render() {
        const blogId = this.props.match.params.id && Number( this.props.match.params.id );
        return (
            <Rpc collection={this.collections.blogCollection}
                 method="blog-info"
                 params={{ id: blogId }}>
                {( data ) => (
                    <div>
                        <DocumentTitle title={`${data.name}`}/>
                        <PostList
                            collection={this.collections.postCollection}
                            blogId={blogId}
                            titlesOnly={this.appState.titlesOnly}
                        />
                    </div>
                )}
            </Rpc>
        )
    }
}