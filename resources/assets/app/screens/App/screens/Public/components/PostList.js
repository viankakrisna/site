import React, { Component, PropTypes } from 'react';
import { observer } from 'mobx-react';
import CollectionFetcher from '../../../shared/components/support/rest/CollectionFetcher';
import DateIcon from '../../../shared/components/DateIcon';
import PostListItem from './PostListItem';

@observer
export default class PostList extends Component {

    static propTypes = {
        collection: React.PropTypes.object,
        language: React.PropTypes.string,
        blogId: React.PropTypes.number
    };

    /**
     * @type {PostCollection}
     */
    postCollection = null;

    /**
     * @type {Date}
     */
    date = null;

    constructor( props ) {
        super( props );
        this.postCollection = props.collection;
    }

    setDateGroup = ( date ) => {
        const pubDate = new Date( this.formatDate( date ) );
        const pd = pubDate.getDate();
        if ( !this.date ) {
            this.date = pubDate;
            return true;
        }
        else if ( pd != this.date.getDate() ) {
            this.date = pubDate;
            return true;
        }
        else {
            return false;
        }
    };

    formatDate( dateStr ) {
        return dateStr.replace( /-/g, '/' );
    }

    belongsToDateGroup( pubDate ) {
        const dateToCheck = new Date( this.formatDate( pubDate ) );
        return dateToCheck.toDateString() === this.date.toDateString();
    };

    render() {
        const { language, blogId } = this.props;

        return (
            <div className="post-list">
                <CollectionFetcher
                    collection={this.postCollection}
                    params={{ language: language, blogId: blogId }}
                    tag="div">

                    { ( model ) => {
                        const newDate = this.setDateGroup( model.get( 'pubdate' ) );
                        return (
                            <div key={model.id}>
                                {newDate ? (
                                        <div className="list-date-group">
                                            <div>
                                                {newDate &&
                                                <DateIcon date={this.date}/> }
                                            </div>
                                            {newDate &&
                                            <div>
                                                {this.postCollection.models.map( ( model ) => {
                                                    if ( this.belongsToDateGroup( model.get( 'pubdate' ) ) ) {
                                                        return (
                                                            <PostListItem
                                                                key={model.id}
                                                                model={model}/>
                                                        );
                                                    }
                                                    return null;
                                                } )}
                                            </div>}
                                        </div>
                                    ) : (
                                        <div/>
                                    )
                                }
                            </div>

                        )
                    }}

                </CollectionFetcher>
            </div>
        )
    }
}
