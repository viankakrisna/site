import React, { Component, PropTypes } from 'react';
import ModalWindow from '../../../shared/components/ModalWindow';

export default class ReportPostModal extends Component {

    state = {
        isOpen: false
    };

    static propTypes = {
        isOpen: PropTypes.bool.isRequired,
        closeCallback: PropTypes.func.isRequired,
        postModel: PropTypes.any
    };

    constructor( props ) {
        super( props );
    }

    componentDidMount() {
        this.setState( {
            isOpen: this.props.isOpen
        } );
    }

    componentWillReceiveProps( nextProps ) {
        if ( this.props.isOpen != nextProps.isOpen ) {
            this.setState( {
                isOpen: nextProps.isOpen
            } );
        }
    }

    onRequestClose = () => {
        this.setState( {
            isOpen: false
        }, () => {
            this.props.closeCallback();
        } );
    };

    render() {
        return (
            <ModalWindow
                isOpen={this.state.isOpen}
                closeCallback={this.onRequestClose}>
                <h1>Report Post</h1>

                {this.props.postModel &&
                <h2>{this.props.postModel.get( 'title' )}</h2>}

                <div>
                    <div>
                        <label>
                            <input
                                type="radio"
                                name="reason"
                                value="Spam"/>
                            Spam
                        </label>
                    </div>
                    <div>
                        <label>
                            <input
                                type="radio"
                                name="reason"
                                value="Personal and confidential information"/>
                            Personal and confidential information
                        </label>
                    </div>
                    <div>
                        <label>
                            <input type="radio"
                                   name="reason"
                                   value="Threatening, harassing, or inciting violence"/>
                            Threatening, harassing, or inciting violence
                        </label>
                    </div>
                    <div>
                        <label>
                            <input
                                type="radio" name="reason"
                                value="Other"/>
                             Other (max 100 characters)
                            <input name="other" type="text" disabled />
                        </label>
                    </div>

                </div>
            </ModalWindow>
        );
    }
}