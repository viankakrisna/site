import React from 'react';
import FeedIcon from '../../../shared/components/FeedIcon';

const PostListItem = ( { model } ) => {
    return (
        <div className="list-item">
            <h3 key={model.id}>
                <a href={model.get( 'link' )}>
                    {model.get( 'title' )}
                </a>
            </h3>
            <div className="details">
                <span>Posted by </span>
                {
                    model.get( 'authors' ) && model.get( 'blog_name' ) != model.get( 'authors' ) &&
                    (
                        <span>
                        <a href={model.get( 'blog_url' )}>
                            {model.get( 'authors' )}
                        </a>
                        <span> in </span>
                    </span>
                    )
                }
                <a href={model.get( 'blog_url' )}>
                    {model.get( 'blog_name' )}
                </a>
                <span> </span>
                <time
                    title={model.get( 'pubdate' )}
                    dateTime={model.get( 'pubdate' )}>
                    {model.get( 'pubdate_for_humans' )}
                </time>
                <span> </span>
                <FeedIcon model={model}/>
            </div>
        </div>
    );
};

export default PostListItem;
