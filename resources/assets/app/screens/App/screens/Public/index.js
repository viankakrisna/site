import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import PostList from './components/PostList';
import DocumentTitle from '../../shared/components/support/DocumentTitle';
import A from '../../shared/components/A';
import Welcome from '../../shared/components/Welcome';

@inject( 'appState' )
@inject( 'collections' )
@observer
export default class Posts extends Component {

    /**
     * @type {AppState}
     */
    appState = null;

    constructor( props ) {
        super( props );
        this.collections = props.collections;
        this.appState = props.appState;
    }

    render() {

        const { language }= this.props.match.params;

        return (
            <div>
                <DocumentTitle title="Planet React" hideHeader={true}/>
                <div style={{marginBottom: '1em', textAlign: 'left'}}>
                    Planet React is an aggregation of blogs and news from the React community.
                </div>

                <PostList
                    collection={this.collections.postCollection}
                    titlesOnly={this.appState.titlesOnly}
                    language={language}
                />
            </div>
        )
    }
}