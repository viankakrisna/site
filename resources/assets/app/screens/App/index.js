import React, { Component } from 'react';
import { Provider, observer } from 'mobx-react';
import DevTools from 'mobx-react-devtools';
import {
    BrowserRouter,
    Route,
    Switch
} from 'react-router-dom';
import AsyncRoute from './shared/components/support/AsyncRoute';
import LoadingBar from './shared/components/support/LoadingBar';
import NavBar from './shared/components/NavBar';
import TopBlogs from './shared/components/TopBlogs';
import TopPosters from './shared/components/TopPosters';
import Planet from './shared/components/Planet';
import Contact from './shared/components/Contact';
import Footer from './shared/components/Footer';
import ProfileMenu from './shared/components/ProfileMenu';
import SubmitBlogModal from './shared/components/SubmitBlogModal';

const hideProfileMenu = document.location.search.match( /pm=0/ );

@observer
export default class App extends Component {
    /**
     * @type {AuthService}
     */
    authService = null;

    /**
     * @type {AppState}
     */
    appState = null;

    /**
     * @type {Object}
     */
    collection = null;

    constructor( props ) {
        super( props );
        this.authService = props.authService;
        this.appState = props.appState;
        this.collections = props.collections;
    }

    render() {

        return (
            <BrowserRouter>
                <Provider ref="provider" {...this.props}>
                    <div>
                        <LoadingBar
                            loading={this.appState.pendingRequests > 0}/>

                        <SubmitBlogModal isOpen={this.appState.isSubmitModaOpen}
                                         closeCallback={this.appState.closeSubmitBlogModal}/>

                        <NavBar appName={this.appState.appName}/>

                        <div className="content">

                            <div className="two-column-layout">
                                <div>
                                    <Route
                                        exact
                                        path="/"
                                        render={( props ) =>
                                            <AsyncRoute {...props}
                                                        component={import('./screens/Public')}/>}
                                    />
                                    <Route
                                        path="/lang/:language"
                                        render={( props ) =>
                                            <AsyncRoute {...props}
                                                        component={import('./screens/Public')}/>}
                                    />
                                    <Route
                                        exact
                                        path="/blogs"
                                        render={( props ) =>
                                            <AsyncRoute {...props}
                                                        component={import('./screens/Public/screens/Blogs')}/>}
                                    />
                                    <Route
                                        path="/blogs/:id"
                                        render={( props ) =>
                                            <AsyncRoute {...props}
                                                        component={import('./screens/Public/screens/Posts')}/>}
                                    />
                                    <Route
                                        path="/faq"
                                        render={( props ) =>
                                            <AsyncRoute {...props}
                                                        component={import('./screens/Public/screens/FAQ')}/>}
                                    />

                                    <Route
                                        exact
                                        path="/admin"
                                        render={( props ) =>
                                            <AsyncRoute {...props}
                                                        auth={{
                                                            allowedRoles: [ 'admin' ]
                                                        }}
                                                        component={import('./screens/Admin')}/>}
                                    />
                                    <Route
                                        path="/admin/blogs"
                                        render={( props ) =>
                                            <AsyncRoute {...props}
                                                        auth={{
                                                            allowedRoles: [ 'admin' ]
                                                        }}
                                                        component={import('./screens/Admin/screens/Blogs')}/>}
                                    />
                                    <Route
                                        path="/admin/fetcher"
                                        render={( props ) =>
                                            <AsyncRoute {...props}
                                                        auth={{
                                                            allowedRoles: [ 'admin' ]
                                                        }}
                                                        component={import('./screens/Admin/screens/Fetcher')}/>}
                                    />
                                    {/*<Redirect to="/"/>*/}
                                </div>
                                <div>
                                    {this.authService.userProfile && !hideProfileMenu && (
                                        <ProfileMenu
                                            userProfile={this.authService.userProfile}
                                            logout={this.authService.logout}/>)
                                    }

                                    <TopPosters
                                        collection={this.collections.topPosterCollection}/>

                                    <TopBlogs
                                        collection={this.collections.topBlogCollection}/>

                                    <Planet
                                        openSubmitBlogModal={this.appState.openSubmitBlogModal}/>

                                    <Contact
                                        email={this.appState.contactEmail}/>
                                </div>
                            </div>

                        </div>

                        <Footer />

                        {/*<DevTools highlightTimeout={5000} />*/}

                    </div>
                </Provider>
            </BrowserRouter>
        )
    }
}
