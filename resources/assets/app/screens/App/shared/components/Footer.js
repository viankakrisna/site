import React from 'react';
const Footer = ( props ) => {
    return (
        <footer>
            <span>  </span>
            <span>Version {__APP_VERSION__} {process.env.NODE_ENV == 'development' && '(development mode)'} - <a
                href={__APP_REPO__}>Source</a></span>
        </footer>
    );
};

export default Footer;