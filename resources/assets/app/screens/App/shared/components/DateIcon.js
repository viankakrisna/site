import React from 'react';

const DateIcon = ( props ) => {
    return (
        <time className="date-icon" dateTime={props.date.toString()}>
            <div className="month">
                {`${props.date.toLocaleDateString( 'en-US', { month: 'short' } )}`}
            </div>
            <div className="day-year">

                <div className="day">
                    {`${props.date.toLocaleDateString( 'en-US', { day: '2-digit' } )}`}
                </div>
                <div className="year">
                    {`${props.date.toLocaleDateString( 'en-US', { year: 'numeric' } )}`}
                </div>
            </div>

        </time>
    )
};

DateIcon.PropTypes = {
    date: React.PropTypes.instanceOf( Date ).isRequired,
};

export default DateIcon;