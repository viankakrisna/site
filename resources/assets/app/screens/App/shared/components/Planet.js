import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Box from './Box'

export default class Planet extends Component {

    static propTypes = {
        openSubmitBlogModal: React.PropTypes.func.isRequired
    };

    constructor( props ) {
        super( props );
    }

    render() {
        return (
            <Box title="Planet" color="white">
                <Link to="/faq">FAQ</Link> Frequently asked questions
                <br/>
                <a href="#" onClick={this.props.openSubmitBlogModal}>Add</a>
                <span> your
                blog to
                Planet React.</span>
                <br/>
                <Link to="/blogs">List</Link> of all subscribed blogs.
            </Box>
        )
    }
}
