import React, { Component } from 'react';
import Logo from './Logo';
import LanguageChooser from './LanguageChooser';

export default class NavBar extends Component {

    static propTypes = {
        appName: React.PropTypes.string.isRequired
    };

    constructor( props ) {
        super( props );
    }

    render() {
        return (
            <div className="navbar">
                <div>
                    <Logo name={this.props.appName}/>
                    <div className="right">
                        <LanguageChooser/>
                    </div>

                </div>
            </div>
        )
    }
}
