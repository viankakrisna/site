import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import LanguageSelector from './LanguageSelector';

const LanguageChooser = withRouter( ( props ) => {
    const onChange = ( event ) => {
        const languagePage = event.target.value == 'en' ? '' : '/lang/' + event.target.value;
        props.history.push( `${languagePage}` );
    };

    const { pathname } = props.location;
    const selectedLanguage = pathname.substr( pathname.lastIndexOf( '/' ) + 1 );
    return (
        <LanguageSelector onChange={onChange} selected={selectedLanguage}/>
    )
} );

export default LanguageChooser;