import React, { Component, PropTypes } from 'react';
import CollectionFetcher from './support/rest/CollectionFetcher';
import { Link } from 'react-router-dom';
import Box from './Box'

export default class TopBlogs extends Component {

    static propTypes = {
        collection: React.PropTypes.object
    };

    /**
     * @type {BaseCollection}
     */
    collection = null;

    constructor( props ) {
        super( props );
        this.collection = props.collection;
    }

    render() {
        return (
            <Box title="Top Blogs" color="white">
                <small>Number of posts in the past month</small>
                <CollectionFetcher
                    collection={this.collection} path="/top" tag="ul">
                    { ( model ) => (
                        <li key={model.id}>
                            <Link to={`/blogs/${model.get('id')}`}>
                                {model.get( 'name' )}
                            </Link>
                            <span> - </span>
                            <span>
                                {model.get( 'post_count' )}
                            </span>
                        </li>
                    )}
                </CollectionFetcher>
            </Box>
        )
    }
}
