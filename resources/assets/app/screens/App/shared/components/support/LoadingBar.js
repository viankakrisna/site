import React from 'react';

const LoadingBar = ( { loading } ) => {
    return (
        <div className={'loading-bar' + (loading ? ' loading' : '') }>
            <div></div>
        </div>
    );
};

export default LoadingBar;