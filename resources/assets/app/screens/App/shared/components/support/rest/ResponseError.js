import React, { Component, PropTypes } from 'react';

export default class ResponseError extends Component {

    static propTypes = {
        error: React.PropTypes.object.isRequired
    };

    constructor( props ) {
        super( props );
        this.error = props.error;
    }

    errorsToArray() {
        let errors = [];
        if ( this.error.body.errors ) {
            const all = this.error.body.errors;
            for ( let key in all ) {
                if ( !all.hasOwnProperty( key ) ) continue;
                const messages = all[ key ];
                messages.forEach( ( message ) => {
                    errors.push( {
                        name: key,
                        message: message
                    } );
                } );
            }
        }
        return errors;
    }

    render() {
        let errors = this.errorsToArray();
        return (
            <div className="error">
                <div>
                    <h5>Error</h5>
                    <div>
                        {this.error.body.message}
                    </div>
                </div>
                {errors &&
                <pre>
                    <ul>
                        {errors.map( ( error, i ) =>
                            <li key={i}>{error.name}: {error.message}</li> )}
                    </ul>
                </pre>
                }
            </div>
        )
    }
}
