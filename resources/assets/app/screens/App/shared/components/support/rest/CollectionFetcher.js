import React, { Component, PropTypes } from 'react';
import { observer, inject } from 'mobx-react';
import ResponseError from './ResponseError';

@inject('appState')
@observer
export default class CollectionFetcher extends Component {

    static propTypes = {
        collection: React.PropTypes.any,
        params: React.PropTypes.object
    };

    /**
     * @type {BaseCollection}
     */
    collection = null;

    constructor( props ) {
        super( props );
        this.appState = props.appState;
        this.collection = props.collection;
    }

    componentWillReceiveProps( nextProps ) {
        let changed = false;
        if (this.props.params) {
            const params = this.props.params;
            const nextParams = nextProps.params;
            for (let key in params) {
                if (!params.hasOwnProperty(key)) {
                    continue;
                }
                if ( nextParams[key] && params[key] !== nextParams[key] ) {
                    changed = true;
                    break;
                }
            }
        }

        if ( nextProps.params && changed ) {
            this.fetch( nextProps.params );
        }
    }

    componentDidMount() {
        const params = this.props.params && this.props.params ? this.props.params : '';
        this.fetch( params );
    }

    componentWillUnmount() {
        this.collection.clear();
        this.collection.cancelRequest();
        this.appState.pendingRequests = 0;
    }

    async fetch( params ) {
        this.appState.pendingRequests++;
        await this.collection.fetch( { data: { params } } );
        this.appState.pendingRequests--;
    }

    render() {
        const { tag, children } = this.props;
        const { request, error } = this.collection;

        const Tag = tag ? `${tag}` : 'div';

        if ( request ) {
            return <div/>;
        }

        if ( error ) {
            return (
                <ResponseError error={error}/>
            )
        }

        if ( this.collection.models.length && !request ) {
            return (
                <Tag>
                    {this.collection.models.map( children )}
                </Tag>
            )
        }
        else {
            return (
                <div>No result</div>
            )
        }
    }
}
