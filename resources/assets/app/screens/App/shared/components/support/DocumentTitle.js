import React, { Component } from 'react';
import { inject, observer } from 'mobx-react'

@inject( 'appState' )
@observer
export default class DocumentTitle extends Component {

    static propTypes = {
        title: React.PropTypes.string.isRequired,
        hideHeader: React.PropTypes.bool
    };

    /**
     * @type {AppState}
     */
    appState = null;

    constructor( props ) {
        super( props );
        this.appState = props.appState;
    }

    componentWillMount() {
        this.appState.updateDocumentTitle( this.props.title );
    }

    componentWillReceiveProps( nextProps ) {
        this.appState.updateDocumentTitle( nextProps.title );
    }

    render() {
        if ( this.props.hideHeader ) {
            return null;
        }
        return (
            <h1>{this.props.title}</h1>
        );
    }
}