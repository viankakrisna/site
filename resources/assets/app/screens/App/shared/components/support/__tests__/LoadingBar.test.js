import React from 'react';
import renderer from 'react-test-renderer';
import LoadingBar from '../LoadingBar';

it( 'renders correctly', () => {
    const tree = renderer.create(
        <LoadingBar loading={true}/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );