import React, { Component, PropTypes } from 'react';
import { observable } from 'mobx';
import { observer, inject } from 'mobx-react';
import ResponseError from './ResponseError';

@inject( 'appState' )
@observer
export default class Rpc extends Component {

    static propTypes = {
        children: React.PropTypes.func.isRequired,
        collection: React.PropTypes.any.isRequired,
        method: React.PropTypes.string.isRequired,
        params: React.PropTypes.object
    };

    /**
     * @type {BaseCollection}
     */
    collection = null;

    @observable response;

    constructor( props ) {
        super( props );
        this.appState = props.appState;
        this.collection = props.collection;
    }

    componentWillReceiveProps( nextProps ) {
        let changed = false;
        if ( this.props.params ) {
            const params = this.props.params;
            const nextParams = nextProps.params;
            for ( let key in params ) {
                if ( !params.hasOwnProperty( key ) ) {
                    continue;
                }
                if ( nextParams[ key ] && params[ key ] !== nextParams[ key ] ) {
                    changed = true;
                    break;
                }
            }
        }
        if ( nextProps.params && changed ) {
            this.rpc( nextProps.params );
        }
    }

    componentDidMount() {
        const params = this.props.params && this.props.params ? this.props.params : '';
        this.rpc( params );
    }

    componentWillUnmount() {
        this.collection.cancelRequest();
        this.appState.pendingRequests = 0;
    }

    async rpc( params ) {
        const method = this.props.method;
        this.appState.pendingRequests++;
        this.response = await this.collection.rpc( method, params );
        this.appState.pendingRequests--;
    }

    render() {
        const { tag } = this.props;
        const { request, error } = this.collection;
        const Tag = tag ? `${tag}` : 'div';

        if ( request ) {
            return <div/>;
        }

        if ( error ) {
            return (
                <ResponseError error={error}/>
            )
        }

        if ( this.response && !request ) {
            return (
                <Tag>
                    {this.props.children.call( this, this.response )}
                </Tag>
            )
        }
        else {
            return (
                <div>No result</div>
            )
        }
    }
}
