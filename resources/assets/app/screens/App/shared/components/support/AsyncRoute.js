import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { observable } from 'mobx'
import { inject, observer } from 'mobx-react'

@inject( 'appState' )
@inject( 'authService' )
@observer
export default class AsyncRoute extends Component {

    /**
     * Indicates if the route has been loaded.
     * @type {boolean}
     */
    @observable isLoaded = false;

    /**
     * @type {AppState}
     */
    appState = null;

    /**
     * @type {AuthService}
     */
    authService = null;

    constructor( props ) {
        super( props );
        this.appState = props.appState;
        this.authService = props.authService;
    }

    componentDidUpdate() {
        window.scrollTo( 0, 0 );
    }

    onLogin = () => {
        this.authService.login( window.location.pathname );
    };

    componentDidMount() {
        const { auth, match } = this.props;
        const { isAuthenticated } = this.authService;
        if ( auth && !isAuthenticated ) {
            this.authService.login( match.url );
        }

        this.appState.pendingRequests++;

        this.props.component.then( ( module ) => {
            this.component = module.default;
            this.appState.pendingRequests--;
            this.isLoaded = true;
        } );
    }

    render() {
        const { isAuthenticated } = this.authService;
        const { auth } = this.props;
        const allowedRoles = auth && auth.allowedRoles;
        const redirectUrl = this.authService.getRedirectUrl();

        if ( auth && !isAuthenticated ) {
            return (
                <div>
                    Requires authentication
                    <p>
                        <a href="#" onClick={this.onLogin}>Log in</a>
                    </p>
                </div>
            )
        }

        if ( isAuthenticated && redirectUrl ) {
            localStorage.removeItem( 'redirect_url' );
            return (
                <Redirect to={redirectUrl}/>
            )
        }

        if ( this.isLoaded === true ) {
            if ( isAuthenticated && allowedRoles ) {
                if ( !this.authService.userHasRole( allowedRoles ) ) {
                    return (
                        <div>
                            Not allowed
                        </div>
                    )
                }
            }
            return <this.component {...this.props} />
        }
        else {
            return null;
        }
    }
}