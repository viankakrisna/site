import React from 'react';

const Box = ( props ) => {
    return (
        <div
            className={'box' + (' color-' + props.color) + (' size-' + props.size) + (' align-' + props.textAlign)}>
            {props.title && <h4>{props.title}</h4>}
            {props.children}
        </div>
    )
};

Box.PropTypes = {
    title: React.PropTypes.string,
    color: React.PropTypes.oneOf( [ 'none', 'blue', 'yellow', 'white' ] ),
    size: React.PropTypes.oneOf( [ 'small', 'medium', 'large' ] ),
    textAlign: React.PropTypes.oneOf( [ 'left', 'center', 'right' ] ),
    children: React.PropTypes.element.isRequired,
};

Box.defaultProps = {
    color: 'none',
    size: 'medium',
    textAlign: 'left'
};

export default Box;