import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import ModalWindow from '../ModalWindow';

it( 'renders correctly', () => {
    const closeCallback = () => {
        void(0);
    };

    const wrapper = shallow(
        <ModalWindow isOpen={true} closeCallback={closeCallback}>
            <div>content</div>
        </ModalWindow>
    );

    expect( wrapper.contains( <div>content</div> ) ).toBe( true );

} );