import React from 'react';
import A from '../A';
import renderer from 'react-test-renderer';

it( 'renders correctly', () => {
    const onClick = ( event ) => {
        void(0);
    };

    const tree = renderer.create(
        <A onClick={onClick}>Link</A>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );