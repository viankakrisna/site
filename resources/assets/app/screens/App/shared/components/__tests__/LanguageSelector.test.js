import React from 'react';
import LanguageSelector from '../LanguageSelector';
import renderer from 'react-test-renderer';

it( 'renders correctly', () => {
    const onChange = ( event ) => {
        void(0);
    };

    const tree = renderer.create(
        <LanguageSelector onChange={onChange} selected="en"/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );