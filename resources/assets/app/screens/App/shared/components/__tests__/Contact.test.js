import React from 'react';
import Contact from '../Contact';
import renderer from 'react-test-renderer';

it( 'renders correctly', () => {
    const tree = renderer.create(
        <Contact email="jrocket@example.com"/>
    ).toJSON();
    expect( tree ).toMatchSnapshot();
} );