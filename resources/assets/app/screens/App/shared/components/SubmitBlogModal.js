import React, { Component, PropTypes } from 'react';
import ModalWindow from './ModalWindow';
import SubmitBlogForm from './SubmitBlogForm';

export default class SubmitBlogModal extends Component {

    state = {
        isOpen: false
    };

    static propTypes = {
        isOpen: PropTypes.bool.isRequired,
        closeCallback: PropTypes.func.isRequired
    };

    constructor( props ) {
        super( props );
    }

    componentDidMount() {
        this.setState( {
            isOpen: this.props.isOpen
        } );
    }

    componentWillReceiveProps( nextProps ) {
        if ( this.props.isOpen != nextProps.isOpen ) {
            this.setState( {
                isOpen: nextProps.isOpen
            } );
        }
    }

    onRequestClose = () => {
        this.setState( {
            isOpen: false
        }, () => {
            this.props.closeCallback();
        } );
    };

    render() {
        return (
            <ModalWindow
                isOpen={this.state.isOpen}
                closeCallback={this.onRequestClose}>
                <h1>Sumbit Blog</h1>
                <SubmitBlogForm/>
            </ModalWindow>
        );
    }
}