import React from 'react';

const FeedIcon = ( { model, style } ) => {
    const url = model.get( 'feed_url' )
        ? model.get( 'feed_url' )
        : model.get( 'blog_url' );
    
    return (
        <a href={url} className="feed-icon" title="Feed" style={style}> </a>
    )
};

FeedIcon.propTypes = {
    model: React.PropTypes.object.isRequired,
    style: React.PropTypes.object
};

export default FeedIcon;