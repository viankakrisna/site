import React, { Component } from 'react';
import { Link } from 'react-router-dom';

const HomeLink = ( props ) => {
    return (
        <Link to="/">← Home</Link>
    )
};

export default HomeLink;