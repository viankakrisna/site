import React, { Component, PropTypes } from 'react';
import Modal from 'react-modal';

const customStyles = {
    overlay: {
        zIndex: 100000
    }
};

export default class ModalWindow extends Component {

    static propTypes = {
        isOpen: PropTypes.bool.isRequired,
        closeCallback: PropTypes.func.isRequired
    };

    state = {
        isOpen: false
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.setState( {
            isOpen: this.props.isOpen
        } );
    }

    componentWillReceiveProps( nextProps ) {
        if ( this.props.isOpen != nextProps.isOpen ) {
            this.setState( {
                isOpen: nextProps.isOpen
            } );
        }
    }

    render() {
        return (
            <Modal
                isOpen={this.state.isOpen}
                style={customStyles}
                onRequestClose={this.props.closeCallback}
                contentLabel="Modal"
            >
                {this.props.children}
            </Modal>
        );
    }
}
