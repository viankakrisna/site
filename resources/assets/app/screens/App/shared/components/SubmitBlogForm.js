import React, { Component, PropTypes } from 'react';
import { observer, inject } from 'mobx-react';
import LanguageSelector from './LanguageSelector';
import ResponseError from './support/rest/ResponseError';

@inject( 'collections' )
@observer
export default class SubmitBlogForm extends Component {
    /**
     * @type {BaseCollection}
     */
    blogCollection = null;

    /**
     * @type {Model}
     */
    model = null;

    constructor( props ) {
        super( props );
        this.blogCollection = props.collections.blogCollection;

        // @todo: just use a model
        this.model = this.blogCollection.add( [ {
            name: '',
            url: '',
            lang: 'en',
            feed_url: '',
            is_rss: 1
        } ] )[ 0 ];
    }

    componentWillUnmount() {
        this.blogCollection.remove( [ this.model.id ] );
        this.blogCollection.error = null;
    }

    onSubmit = ( event ) => {
        event.preventDefault();
        this.blogCollection.error = null;
        this.blogCollection.create( this.model );
    };

    onChange = ( event ) => {
        this.model.set( {
            [event.target.name]: event.target.value
        } );
        console.log(this.model.get('lang'));
    };

    onHasFeed = ( event ) => {
        const checked = event.target.checked;
        this.model.set( {
            'is_rss': checked ? 1 : 0,
            'feed_url': checked ? '' : ''
        } );
    };

    render() {
        const { error } = this.model.collection;
        const { request } = this.model.collection;
        const disableButton = request && !error;

        return (
            <div>
                {error && <ResponseError error={error}/>}

                {!this.model.isNew ? (
                        <div>Thank you for submitting the blog! Aggregation will
                            start once the feed has been approved.</div>

                    ) : (
                        <div>
                            <form onSubmit={this.onSubmit}>
                                <div className="form-row">
                                    <label>Name:</label>
                                    <input autoFocus required name="name"
                                           type="text"
                                           value={this.model.get( 'name' )}
                                           onChange={this.onChange}/>
                                </div>
                                <div className="form-row">
                                    <label>Language: </label>
                                    <LanguageSelector onChange={this.onChange} selected={this.model.get('lang')}/>
                                </div>
                                <div className="form-row">
                                    <label>Blog URL:</label>
                                    <input name="url"
                                           type="url"
                                           required
                                           value={this.model.get( 'url' )}
                                           onChange={this.onChange}/>
                                </div>
                                <div className="form-row">
                                    <label>Has feed? </label>
                                    <input name="is_rss"
                                           type="checkbox"
                                           checked={this.model.get( 'is_rss' ) == 1}
                                           onChange={this.onHasFeed}/>
                                </div>
                                <div className="form-row"
                                     style={{ display: this.model.get( 'is_rss' ) == 1 ? '' : 'none' }}>
                                    <label>Feed URL:</label>
                                    <input name="feed_url"
                                           type="url"
                                           value={this.model.get( 'feed_url' )}
                                           onChange={this.onChange}/>
                                </div>
                                <p>
                                    <button
                                        disabled={disableButton}>{disableButton ? 'Submitting...' : 'Submit'}</button>
                                </p>

                            </form>
                        </div>

                    )}

            </div>
        )

    }
}