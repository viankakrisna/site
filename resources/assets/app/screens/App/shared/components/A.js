import React, { Component } from 'react';

const A = ( props ) => {
    const onClick = ( event ) => {
        event.preventDefault();
        props.onClick();
    };
    return (
        <a href="#" onClick={onClick}>
            {props.children}
        </a>
    )
};

A.propTypes = {
    onClick: React.PropTypes.func.isRequired
};

export default A;