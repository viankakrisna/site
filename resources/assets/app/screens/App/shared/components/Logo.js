import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router-dom';
const Logo = ( props ) => {
    return (
        <Link to="/" className="logo">
            {props.name}
        </Link>
    )
};
Logo.PropTypes = {
    name: PropTypes.string
};
Logo.defaultProps = {
    name: 'App'
};
export default Logo;