import React from 'react';
import Box from './Box'

const Contact = ( props ) => {
    return (
        <Box title="Contact" color="white">
            Get in touch with the Planet React administrators at {props.email}
        </Box>
    )
};

Contact.propTypes = {
    email: React.PropTypes.string.isRequired
};

export default Contact;