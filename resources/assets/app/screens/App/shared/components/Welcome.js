import React from 'react';
import Box from './Box'

const About = () => {
    return (
        <Box color="blue" size="small" textAlign="left">
            Planet React is an aggregation of blogs and news from the React community.
        </Box>
    )
};

export default About;