import React, { Component, PropTypes } from 'react';
import CollectionFetcher from './support/rest/CollectionFetcher';
import FeedIcon from './FeedIcon';

import { Link } from 'react-router-dom';

export default class BlogList extends Component {

    /**
     * @type {BlogCollection}
     */
    blogCollection = null;

    constructor( props ) {
        super( props );
        this.blogCollection = props.blogCollection;
    }

    render() {
        return (
            <div>
                <CollectionFetcher
                    collection={this.blogCollection} tag="ul">
                    { ( model ) => (
                        <li key={model.id}>
                            <Link to={`/blogs/${model.get( 'id' )}`}>
                                {model.get( 'name' )}
                            </Link>
                            <FeedIcon model={model} style={{verticalAlign: 'middle'}}/>
                        </li>
                    )}
                </CollectionFetcher>
            </div>
        )
    }
}
