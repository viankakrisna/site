import React from 'react';
import { Link } from 'react-router-dom';
import Box from './Box'

const ProfileMenu = ( props ) => {
    const { userProfile } = props;
    return (
        <Box title={userProfile.name} color="blue">
            <ul>
                {userProfile.roles.indexOf( 'admin' ) > -1 &&
                <li>
                    <Link to="/admin">Admin</Link>
                    <ul>
                        <li>
                            <Link to="/admin/blogs">Manage Blogs</Link>
                        </li>
                        <li>
                            <Link to="/admin/fetcher">Fetcher</Link>
                        </li>
                    </ul>
                </li>}
            </ul>
            <div style={{textAlign: 'right'}}>
                &#123; <a href="#" onClick={props.logout}>Sign Out</a> &#125;
            </div>

        </Box>
    );
};

ProfileMenu.propTypes = {
    userProfile: React.PropTypes.object.isRequired,
    logout: React.PropTypes.func.isRequired
};

export default ProfileMenu;