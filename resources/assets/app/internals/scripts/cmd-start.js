require( 'dotenv' ).config();

const exec = require( './utils/exec' );
const exitHandler = require('./utils/exitHandler');
const getProcessLine = require('./utils/getProcessLine');

// Check if the php server is already running.
const phpServProcessLine = getProcessLine( 'php -S 127.0.0.1:' + process.env.PLANET_SERVER_DEVELOPMENT_PORT );
if ( phpServProcessLine ) {
    const pid = phpServProcessLine.trim().match( /^\d+/ )[ 0 ];
    console.log( '' );
    console.log( '  PHP server is already running' );
    console.log( '' );
    console.log( '  Use `kill -9 ' + pid + '` to terminate the process' );
    console.log( '' );

    exitHandler( { exit: true } );
}

process.stdin.resume(); //so the program will not close instantly

// Listen for exits
process.on( 'exit', exitHandler.bind( null, { cleanup: true } ) );
process.on( 'SIGINT', exitHandler.bind( null, { exit: true } ) );
process.on( 'uncaughtException', exitHandler.bind( null, { exit: true } ) );

// Start the servers.
console.log( 'Staring PHP server.' );
exec( 'nohup php artisan serv --host 127.0.0.1 > /dev/null 2>&1 &' );
console.log( 'Staring Node dev server.' );
exec( 'node devserver.js' );

