const exec = require( './exec' );
const SEPARATOR = '|';

function parseGitLog( noOfEntries = 5 ) {
    let lines = exec( `git log -${noOfEntries} --pretty=format:'%H|%cn|%ct|%s'` ).toString().split( '\n' );
    let result = [];

    for ( let line of lines ) {
        result.push( createObjRow( line ) );
    }
    return result;
}

function createCsvRow( rowObj ) {
    return `${rowObj.hash}${SEPARATOR}${rowObj.committer}${SEPARATOR}${rowObj.commit_timestamp}${SEPARATOR}${rowObj.subject}`;
}

function createObjRow( line ) {
    if ( !line ) {
        return false;
    }
    const columns  = line.split( SEPARATOR );
    return {
        hash: columns[ 0 ],
        committer: columns[ 1 ],
        commit_timestamp: columns[ 2 ],
        subject: columns[ 3 ]
    };
}

function toCsv( logObj ) {
    let result = [];
    for ( let row of logObj ) {
        result.push(
            createCsvRow( row )
        );
    }
    return result.join( '\n' );
}

module.exports = {
    parseGitLog,
    createObjRow,
    createCsvRow,
    toCsv
};