const ora = require( 'ora' );

function log( val ) {

    const d = new Date();
    let message = '';
    if ( val instanceof Buffer ) {
        message = val.toString( 'utf8' ).trim().replace( '\n', '' );
    }
    else if ( val instanceof Array || val instanceof Object ) {
        message = JSON.stringify( val );
    }
    else {
        message = String( val );
    }

    // const spinner = ora('Loading unicorns').start();
    if ( message ) {
        process.stdout.write( `[${d.toJSON()}] ${message}\n` );
    }
}

module.exports = log;