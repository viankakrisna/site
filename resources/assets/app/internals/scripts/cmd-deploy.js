const dotenv = require( 'dotenv' ).config();
const yesno = require( 'yesno' );
const exec = require( './utils/exec' );
const log = require( './utils/log' );
const File = require( './utils/File' );
const git = require( './utils/git' );
const packageJson = require( '../../../../../package.json' );
const Rsync = require( 'rsync' );
const ora = require( 'ora' );

const LOCAL_DIR = process.cwd();
const LOCAL_DEPLOY_LOG = LOCAL_DIR + '/deploy/.log';

const HOST = dotenv.PLANET_DEPLOY_REMOTE_HOST;
const REMOTE_WEB_ROOT = dotenv.PLANET_DEPLOY_REMOTE_WEB_ROOT;
const REMOTE_DEPLOY_LOG = REMOTE_WEB_ROOT + '/deploy/.log';

const args = process.argv.slice( 2 );
const skipBuild = args.indexOf( '--skip-build' ) > -1;
const skipTests = args.indexOf( '--skip-tests' ) > -1;
const force = args.indexOf( '-F' ) > -1;

function exit() {
    process.exit();
}

if ( !exec( 'git diff-index --quiet HEAD -- || echo "untracked"' ) == '' ) {
    log( 'Repo has uncommitted changes. Please commit the changes before deploying' );
    process.exit();
}

const latestCommits = git.parseGitLog( 5 ).reverse();
log( 'Latest commits:' );
for ( let i = 0; i < latestCommits.length; i++ ) {
    const commit = latestCommits[ i ];
    const commitDate = new Date( commit.commit_timestamp * 1000 );
    log( `${i + 1}) ${commitDate.toLocaleString( 'en-US', { hour12: false } )}` );
    log( `   ${commit.committer}: ${commit.subject}` );
    log( ' ' );
}

if ( !force ) {
    yesno.ask( `\nThis will deploy ${packageJson.name} ${packageJson.version} Continue? (Y/n)`, true, function ( ok ) {
        if ( ok ) {
            initDeployment();
        } else {
            exit();
        }
    } );
}
else {
    init();
}

function initDeployment() {
    log( 'Fetching deploy log from remote' );
    fetchRemoteDeployLog().then( ( success ) => {
        if ( !success ) {
            log( `Something went wrong fetching the remote deploy log. Please investigate` );
            exit();
        }

        const latestCommit = git.parseGitLog( 1 );

        const deployed = File.getLines( LOCAL_DEPLOY_LOG );
        const lastDeployed = git.createObjRow( deployed[ deployed.length - 1 ] );

        if ( (latestCommit[ 0 ] && lastDeployed) && (latestCommit[ 0 ].hash == lastDeployed.hash ) ) {
            log( 'No changes' );
            exit();
        }

        runTests();

        build();

        deploy().then( ( success ) => {
            if ( success ) {
                syncLogs().then( ( result ) => {
                    exit();
                } );
            }
        } ).catch( () => {
            exit();
        } );

    } ).catch( ( err ) => {
        log( 'Something went wrong' );
        log( `rsync error ${err.code}` );
        exit();
    } );
}

function runTests() {
    if ( skipTests ) {
        return;
    }
    log( 'Running tests' );

    try {
        exec( 'phpunit' );
    } catch ( ex ) {
        log( 'PHP Unit failed' );
        process.exit();
    }

    try {
        exec( 'yarn test' );
    } catch ( ex ) {
        log( 'Jest tests failed' );
        process.exit();
    }
}

function build() {
    if ( skipBuild ) {
        return;
    }
    log( 'Building app' );
    exec( 'yarn run build' );
}

function deploy() {
    return new Promise( ( resolve, reject ) => {
        const rsync = new Rsync()
            .flags( 'avz' )
            .shell( 'ssh' )
            .set( 'inplace' )
            .delete()
            .exclude( [
                '.git',
                '.gitignore',
                '.idea',
                '.DS_Store',
                'deploy/',
                'storage',
                'vendor',
                '/node_modules',
                '.env'
            ] )
            .source( LOCAL_DIR + '/*' )
            .destination( HOST + ':' + REMOTE_WEB_ROOT )
            .output(
                [ log, log ]
            );

        rsync.execute( function ( error, code, cmd ) {
            if ( error ) {
                reject( error );
            }
            else {
                resolve( true );
            }

        } );
    } );
}

function fetchRemoteDeployLog() {
    return new Promise( ( resolve, reject ) => {
        const rsync = new Rsync()
            .flags( 'avz' )
            .shell( 'ssh' )
            .delete()
            .quiet()
            .set( 'progress' )
            .source( HOST + ':' + REMOTE_DEPLOY_LOG )
            .destination( LOCAL_DEPLOY_LOG )
            .output(
                [ log, log ]
            );
        rsync.execute( function ( error, code, cmd ) {
            if ( error ) {
                reject( { error, code } );
            }
            else {
                resolve( File.exists( LOCAL_DEPLOY_LOG ) );
            }
        } );
    } );
}

function syncLogs() {
    log( 'Syncing logs' );
    let logLines = File.getLines( LOCAL_DEPLOY_LOG );
    const latestCommit = git.parseGitLog( 1 )[ 0 ];
    logLines.push( git.createCsvRow( latestCommit ) );

    File.write( LOCAL_DEPLOY_LOG, logLines.join( '\n' ).trim() );

    return new Promise( ( resolve, reject ) => {
        const rsync = new Rsync()
            .flags( 'avz' )
            .shell( 'ssh' )
            .delete()
            .quiet()
            .set( 'progress' )
            .source( LOCAL_DEPLOY_LOG )
            .destination( HOST + ':' + REMOTE_DEPLOY_LOG )
            .output(
                [ log, log ]
            );
        rsync.execute( function ( error, code, cmd ) {
            if ( error ) {
                reject( error );
            }
            else {
                resolve( true );
            }
        } );
    } );
}

