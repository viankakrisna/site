import { Collection, Model } from 'mobx-rest'

/**
 * @extends Collection
 *
 * @todo: mobx-rest
 *          - primaryKey on model
 *          - fetch with path
 */
export default class BaseCollection extends Collection {

    /**
     * @type {string}
     * @private
     */
    _url = '';

    /**
     * @type {Model}
     * @private
     */
    _model = null;

    constructor( url, model = Model ) {
        super( [] );
        this._url = url;
        this._model = model;
    }

    /**
     * @inheritDoc
     * @returns {string}
     */
    url() {
        return this._url;
    }

    /**
     * @inheritDoc
     * @returns {Model}
     */
    model() {
        return this._model;
    }

    /**
     * Clears the collection.
     */
    clear() {
        this.models.clear();
    }

    /**
     * Cancels the request.
     */
    cancelRequest() {
        if ( this.request ) {
            this.request.abort();
        }
    }

}