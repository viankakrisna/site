import axiosAdapter from '../axiosAdapter';

describe( 'axiosAdapter', () => {

    const adapter = axiosAdapter( {
        xsrfHeaderName: 'X-XSRF-TOKEN',
        withCredentials: true
    } );

    it( 'has an axios instance', () => {
        const instance = adapter.axios;
        expect( instance ).toBeDefined();
        expect( instance.request ).toBeDefined();
    } );

    it( 'supports all verbs', () => {
        expect( adapter.get ).toBeDefined();
        expect( adapter.post ).toBeDefined();
        expect( adapter.put ).toBeDefined();
        expect( adapter.del ).toBeDefined();
    } );

} );
