import BaseCollection from '../BaseCollection';

describe( 'BaseCollection', () => {

    const baseCollection = new BaseCollection( '/api/users' );

    it( 'returns the url', () => {
        expect( baseCollection.url() ).toBe( '/api/users' );
    } );

    it( 'returns the model', () => {
        expect( baseCollection.model() ).toBeDefined();
    } );

    it( 'clears the collection', () => {
        baseCollection.add( [
            { name: 'Curtis' },
            { name: 'Ian' },
            { name: 'Agatha' },
        ] );

        expect( baseCollection.models.length ).toBe( 3 );
        baseCollection.clear();
        expect( baseCollection.models.length ).toBe( 0 );
    } );

    it( 'should have cancelRequest() method', () => {
        expect( baseCollection.cancelRequest ).toBeDefined();
    } );

} );
