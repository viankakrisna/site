import { apiClient } from 'mobx-rest'
import axiosAdapter from './axiosAdapter';
import BaseCollection from './BaseCollection';

// Set up the api client.
const client = apiClient( axiosAdapter( {
    xsrfHeaderName: 'X-XSRF-TOKEN',
    withCredentials: true
} ), { apiPath: '/api' } );

client.axios.interceptors.request.use(function (config) {
    config.headers[ 'Authorization' ] = 'Bearer ' + localStorage.getItem( 'id_token');
    return config;
}, function (error) {
    return Promise.reject(error);
});

client.axios.interceptors.response.use( function ( response ) {
    return response.data.data;
}, function ( error ) {
    return Promise.reject( error );
} );

const postCollection = new BaseCollection( '/posts' );
const blogCollection = new BaseCollection( '/blogs' );
const topBlogCollection = new BaseCollection( '/blogs/top-blogs' );
const topPosterCollection = new BaseCollection( '/posts/top-posters' );
const adminBlogCollection = new BaseCollection( '/admin/blogs' );
const adminStatsCollection = new BaseCollection( '/admin/stats' );
const adminLogCollection = new BaseCollection( '/admin/logs' );
const adminCronCollection = new BaseCollection( '/admin/cron' );

export default {
    postCollection,
    blogCollection,
    topBlogCollection,
    topPosterCollection,
    adminBlogCollection,
    adminStatsCollection,
    adminLogCollection,
    adminCronCollection
};