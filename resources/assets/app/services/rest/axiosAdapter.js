import axios, { CancelToken } from 'axios'

let axiosInstance;

function ajax( url, options ) {
    // Make sure the authorization header is always set.

    const xhr = axiosInstance( url, options );
    const promise = new Promise( ( resolve, reject ) => {
        xhr
            .then( resolve )
            .catch( ( error ) => {
                return reject( error.response.data || {} );
            } );
    } );

    const source = CancelToken.source();
    const abort = ( reason ) => source.cancel( reason );

    return { abort, promise };
}

/**
 * @param axiosOptions
 * @returns {axios, apiPath, get, post, put, del}
 */
export default function axiosAdapter( axiosOptions = {} ) {

    // Create a new axios instance
    axiosInstance = axios.create( axiosOptions );

    return {
        axios: axiosInstance,
        apiPath: '',

        get ( path, data = {}, options = {} ) {
            const { params } = data;
            delete data.params;

            return ajax(
                `${this.apiPath}${path}`,
                Object.assign( {}, { method: 'GET', data, params }, options )
            );
        },

        post( path, data = {}, options = {} ) {
            return ajax(
                `${this.apiPath}${path}`,
                Object.assign( {}, { method: 'POST', data }, options )
            )
        },

        put( path, data = {}, options = {} ) {
            return ajax(
                `${this.apiPath}${path}`,
                Object.assign( {}, { method: 'PUT', data }, options )
            )
        },

        del( path, options = {} ) {
            return ajax(
                `${this.apiPath}${path}`,
                Object.assign( {}, { method: 'DELETE' }, options )
            )
        }
    }
}


