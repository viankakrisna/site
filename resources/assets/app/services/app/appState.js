import { observable, action, autorun } from 'mobx';

class AppState {

    appName = 'Planet React';

    version = '1.0.0';

    contactEmail = 'planet-react [at] mr-andersen.no';

    @observable pendingRequests = 0;

    @observable documentTitle = '';

    @observable isSubmitModaOpen = false;

    @observable titlesOnly = false;

    constructor() {

        this.titlesOnly = window.localStorage.getItem( 'post_titles_only' ) == 'true';

        autorun( () => document.title = this.documentTitle );

        autorun( () => {
            window.localStorage.setItem( 'post_titles_only', this.titlesOnly );
        } );

        // autorun( () => {
        //     console.log(this.pendingRequests)
        // } );
    }

    // @todo: rename -> onTogglePostTitles
    @action onToggleTitles = () => {
        this.titlesOnly = !Boolean(this.titlesOnly);
    };

    @action updateDocumentTitle( title ) {
        this.documentTitle = `${title ? title + ' - ' : ''}${this.appName}`;
    }

    @action openSubmitBlogModal = () => {
        this.isSubmitModaOpen = true;
    };

    @action closeSubmitBlogModal = () => {
        this.isSubmitModaOpen = false;
    };

}

const singleton = new AppState();

export default singleton;

