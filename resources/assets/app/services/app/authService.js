import Auth0Lock from 'auth0-lock'
import { observable, action, autorun } from 'mobx';
import { isTokenExpired } from './jwtHelper'

class AuthService {

    @observable isAuthenticated = false;

    @observable userProfile = null;

    lock = null;

    constructor( clientId, domain, redirectUrl ) {
        this.lock = new Auth0Lock( clientId, domain, {
            auth: {
                redirectUrl: redirectUrl,
                responseType: 'token'
            },
            allowForgotPassword: false
        } );

        this.isAuthenticated = this.loggedIn();

        if ( this.isAuthenticated ) {
            this._doAuthentication( this.getToken() );
        }

        this.lock.on( 'hide', () => {
            this.deleteRedirectUrl();
        } );
        this.lock.on( 'authenticated', ( { idToken } ) => {
            this._doAuthentication( idToken )
        } );

        this.login = this.login.bind( this );
    }

    _doAuthentication( idToken ) {
        this.setToken( idToken );
        this.getProfile( idToken ).then( ( profile ) => {
            this.userProfile = profile;
            this.isAuthenticated = true;

        } ).catch( ( error ) => {
            this.logout();
        } );
    }

    login( redirectPath ) {
        this.storeRedirectUrl( redirectPath );
        this.lock.show();
    }

    /**
     * @param token
     * @returns {Promise}
     */
    getProfile( token ) {
        return new Promise( ( resolve, reject ) => {
            this.lock.getProfile( token, ( error, profile ) => {
                if ( error ) {
                    reject( error );
                } else {
                    resolve( profile );
                }
            } );
        } );
    }

    loggedIn() {
        const token = this.getToken();
        return !!token && !isTokenExpired( token );
    }

    userHasRole( allowedRoles ) {
        if ( !this.userProfile ) {
            return false;
        }

        const { roles } = this.userProfile;
        for ( let i = 0; i < roles.length; i++ ) {
            if ( allowedRoles.indexOf( roles[ i ] ) > -1 ) {
                return true;
            }
        }
        return false;
    }

    storeRedirectUrl( url ) {
        window.localStorage.setItem( 'redirect_url', url );
    }

    getRedirectUrl() {
        return window.localStorage.getItem( 'redirect_url' );
    }

    deleteRedirectUrl() {
        return window.localStorage.removeItem( 'redirect_url' );
    }

    setToken( idToken ) {
        // Saves user token to local storage
        window.localStorage.setItem( 'id_token', idToken );
    }

    getToken() {
        // Retrieves the user token from local storage
        return window.localStorage.getItem( 'id_token' );
    }

    logout = () => {
        this.userProfile = null;
        this.isAuthenticated = false;
        window.localStorage.removeItem( 'id_token' );
    };
}

const singleton = new AuthService( __AUTH_CLIENT_ID__, __AUTH_DOMAIN__, __AUTH_CLIENT_REDIRECT_URL__ );

export default singleton;