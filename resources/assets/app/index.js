require( './assets/styles/main.scss' );

import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from './screens/App';

import authService from './services/app/authService';
import appState from './services/app/appState';
import collections from './services/rest';

const props = {
    authService,
    appState,
    collections
};

/**
 * App entry
 */
(async function () {

    // if ( !authService.isAuthenticated ) {
    //
    //     let tries = 0;
    //     const tick = setInterval( () => {
    //         if ( authService.isAuthenticated ) {
    //             clearInterval( tick );
    //             document.location.href = '/';
    //         }
    //
    //         tries++;
    //         if ( tries > 3 ) {
    //             clearInterval( tick );
    //             authService.login( window.location.pathname );
    //         }
    //     }, 500 );
    //
    //     return new Promise( ( resolve ) => {
    //         resolve( 1 );
    //     } );
    // }

    // Start the application.
    // Renders the App to the #root element.
    ReactDOM.render(
        <AppContainer>
            <App {...props}/>
        </AppContainer>,
        document.getElementById( 'root' )
    );

    // Hot Module Replacement API
    if ( module.hot ) {
        module.hot.accept( './screens/App', () => {
            const NextApp = require( './screens/App' ).default;
            ReactDOM.render(
                <AppContainer>
                    <NextApp {...props}/>
                </AppContainer>
                ,
                document.getElementById( 'root' )
            );
        } );
    }
}());
