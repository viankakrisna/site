require( 'dotenv' ).config();

const path = require( 'path' );
const webpack = require( 'webpack' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const ExtractTextPlugin = require( 'extract-text-webpack-plugin' );
const precss = require( 'precss' );
const autoprefixer = require( 'autoprefixer' );
const packageJson = require( './package.json' );

const HappyPack = require( 'happypack' );
const happyThreadPool = HappyPack.ThreadPool( { size: 5 } );

/**
 * Webpack configuration for production.
 * See .env file for constants.
 *
 * https://webpack.js.org/
 */
module.exports = {
    /**
     * Cache generated modules and chunks
     */
    cache: true,
    /**
     * webpack creates a graph of all of your application's dependencies.
     * The starting point of this graph is known as an entry point.
     * The entry point tells webpack where to start and follows the graph of
     * dependencies to know what to bundle. You can think of your application's
     * entry point as the contextual root or the first file to kick off your app.
     *
     * https://webpack.js.org/concepts/#entry
     */
    entry: {
        /**
         * Vendor bundle.
         * Add packages that should be added a separate vendor bundle.
         */
        vendor: [
            'react',
            'react-dom',
            'react-router',
            'mobx',
            'mobx-react'
        ],

        /**
         * Application bundle.
         */
        app: [
            'babel-polyfill',
            path.resolve( __dirname, process.env.PLANET_APP_ENTRY )
        ]
    },

    /**
     * Once you've bundled all of your assets together, we still need to tell
     * webpack where to bundle our application. The webpack output property
     * describes to webpack how to treat bundled code.
     *
     * https://webpack.js.org/concepts/#output
     */
    output: {
        /**
         * Path to where the static assets should be created.
         */
        path: path.resolve( __dirname, process.env.PLANET_JS_OUTPUT ),

        /**
         * publicPath specifies the public URL of the output directory when
         * referenced in a browser.
         *
         * https://webpack.js.org/configuration/output/#output-publicpath
         */
        publicPath: process.env.PLANET_PUBLIC_PATH,

        /**
         * This option determines the name of each output bundle.
         * The bundle is written to the directory specified by the output.path option.
         *
         * https://webpack.js.org/configuration/output/#output-filename
         */
        filename: '[name].[hash].js',

        /**
         * The filename of non-entry chunks as a relative path inside the
         * output.path directory.
         *
         * https://webpack.js.org/concepts/output/#output-chunkfilename
         */
        chunkFilename: '[name].[chunkhash].js'
    },

    /**
     * This option controls if and how Source Maps are generated.
     *
     * cheap-module-source-map
     * SourceMap without column-mappings.
     * SourceMaps from loaders are simplified to a single mapping per line.
     *
     * https://webpack.js.org/configuration/devtool/
     */
    devtool: 'cheap-module-source-map',

    /**
     * Options in .module determine how the different types of modules within a
     * project will be treated during creation.
     *
     * https://webpack.js.org/concepts/#loaders
     */
    module: {
        /**
         * An array of Rules which are matched to requests when modules are created.
         *
         * https://webpack.js.org/configuration/module/#module-rules
         */
        rules: [
            /**
             * JavaScript
             *
             * This rule transpiles ES6 and ES7 code to JavaScript
             * see also /.babelrc for options.
             *
             * https://github.com/babel/babel-loader
             */
            {
                test: /\.js$/,
                include: path.resolve( __dirname, process.env.PLANET_APP_ENTRY ),
                exclude: /node_modules/,
                loader: [ 'happypack/loader?id=js' ],
            },

            /**
             * Fonts
             *
             * Creates a Data Url of font files when module is created.
             */
            {
                test: /\.(woff|woff2|eot|ttf)$/,
                loader: 'url-loader?limit=100000'
            },

            /**
             * ExtractTextPlugin
             * Transpile Sass files
             */
            {
                test: /\.scss$/i,
                loader: ExtractTextPlugin.extract( [
                    'css-loader',
                    'postcss-loader',
                    'resolve-url-loader',
                    'sass-loader?sourceMap'
                ] )
            },

            /**
             * Image files
             *
             * Creates a Data Url of image files when module is created.
             */
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'file-loader?hash=sha512&digest=hex&name=app/[hash].[ext]',
                    'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            },

            /**
             * JSON files
             */
            {
                test: /\.json$/, loader: 'json-loader'
            }
        ]
    },

    /**
     * Since rules only execute transforms on a per-file basis, plugins are most
     * commonly used (but not limited to) performing actions and custom
     * functionality on "compilations" or "chunks" of your bundled modules
     * (and so much more).
     *
     * https://webpack.js.org/concepts/#plugins
     */
    plugins: [

        new webpack.optimize.AggressiveMergingPlugin(),

        createHappyPlugin( 'js', [ 'babel-loader?cacheDirectory=true' ] ),

        /**
         * Injects variables into the application.
         */
        new webpack.DefinePlugin( {
            '__APP_VERSION__': JSON.stringify( packageJson.version ),
            '__APP_USER_AGENT__': JSON.stringify( process.env.PLANET_FETCHER_USER_AGENT ),
            '__APP_REPO__': JSON.stringify( packageJson.repository.url ),
            '__AUTH_DOMAIN__': JSON.stringify( process.env.PLANET_AUTH0_DOMAIN ),
            '__AUTH_CLIENT_ID__': JSON.stringify( process.env.PLANET_AUTH0_CLIENT_ID ),
            '__AUTH_CLIENT_REDIRECT_URL__': JSON.stringify( process.env.PLANET_AUTH0_REDIRECT_URL_PROD ),
            'process.env': {
                NODE_ENV: JSON.stringify( 'production' )
            }
        } ),

        /**
         * Prints more meaningful module names in the browser console on updates.
         */
        new webpack.NamedModulesPlugin(),

        /**
         * Assign the module and chunk ids by occurrence count.
         * Ids that are used often get lower (shorter) ids. This make ids predictable, reduces total file size and is recommended.
         */
        new webpack.optimize.OccurrenceOrderPlugin( true ),

        /**
         * Add options to loaders.
         */
        new webpack.LoaderOptionsPlugin( {
            test: /\.scss$/,
            debug: true,
            options: {
                postcss: function () {
                    return [ precss, autoprefixer ];
                },
                context: path.resolve( __dirname, process.env.PLANET_APP_ENTRY ),
                output: {
                    path: path.resolve( __dirname, process.env.PLANET_JS_OUTPUT )
                }
            }
        } ),

        /**
         * Identify common modules and put them in a vendor chunk.
         */
        new webpack.optimize.CommonsChunkPlugin( {
            name: 'vendor',
            minChunks: Infinity
        } ),

        /**
         * Minimize JavaScript code.
         */
        new webpack.optimize.UglifyJsPlugin( {
            compress: {
                warnings: false
            }
        } ),

        /**
         * Move every require("style.css") in entry chunks into a separate css output file.
         */
        new ExtractTextPlugin( '../../css/styles.css' ),

        /**
         * Create HTML template
         */
        new HtmlWebpackPlugin( {
            title: '',
            hash: true,
            inject: false,
            template: path.resolve( process.env.PLANET_HTML_TEMPLATE ),
            filename: path.resolve( process.env.PLANET_HTML_TEMPLATE_OUTPUT )
        } )
    ],
};


function createHappyPlugin( id, loaders ) {
    return new HappyPack( {
        id: id,
        loaders: loaders,
        threadPool: happyThreadPool,

        // disable happy caching with HAPPY_CACHE=0
        cache: process.env.HAPPY_CACHE === '1',

        // make happy more verbose with HAPPY_VERBOSE=1
        verbose: process.env.HAPPY_VERBOSE === '1',
    } );
}