<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create( 'blogs', function ( Blueprint $table ) {
            $table->increments( 'id' );
            $table->string( 'name' );
            $table->string( 'url' )->nullable();
            $table->string( 'feed_url' )->nullable();
            $table->boolean( 'is_rss' )->default( 0 );
            $table->string( 'posts_xpath' )->nullable();
            $table->string( 'post_title_xpath' )->nullable();
            $table->string( 'post_description_xpath' )->nullable();
            $table->string( 'post_pubdate_xpath' )->nullable();
            $table->string( 'post_link_xpath' )->nullable();
            $table->dateTime( 'last_fetched_at' )->nullable();
            $table->boolean( 'failed' )->default( 0 );
            $table->decimal('execution_time', 25, 18)->nullable();
            $table->boolean( 'approved' )->default( 0 );
            $table->timestamps();
            $table->softDeletes();
        } );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop( 'blogs' );
    }
}
